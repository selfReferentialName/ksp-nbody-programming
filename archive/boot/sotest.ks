// sub-orbital test launch script
CLEARSCREEN.
PRINT "SUBORBITAL TEST LAUNCH STARTED".

// GET SPECIFIC IMPULSE
FUNCTION ISP {
	LIST ENGINES IN ENGINELIST.
	DECLARE THRUSTSUM TO 0.
	DECLARE ISPSUM TO 0.
	FOR ENG IN ENGINELIST {
		SET THRUSTSUM TO THRUSTSUM + ENG:THRUST.
		SET ISPSUM TO ISPSUM + ENG:THRUST * ENG:ISP.
	}.
	IF THRUSTSUM = 0 {
		RETURN 0.
	} ELSE {
		RETURN ISPSUM / THRUSTSUM.
	}.
}.

// PRINT FLOATING POINT VALUE AT GIVEN WIDTH
FUNCTION FWIDTH {
	PARAMETER VALUE.
	PARAMETER WIDTH.

	RETURN (VALUE + ""):PADLEFT(WIDTH):SUBSTRING(0, WIDTH).
}.

// VECTOR WITH VALUES AT GIVEN WIDTH
FUNCTION VWIDTH {
	PARAMETER VALUE.
	PARAMETER WIDTH.

	RETURN FWIDTH(VALUE:X, WIDTH) + FWIDTH(VALUE:Y, WIDTH) + FWIDTH(VALUE:Z, WIDTH).
}.

// LOG A POINT OF TELEMETRY
FUNCTION LOGSTEP {
	PARAMETER NEWSTAGE.

	DECLARE NEWSTAGEVALUE TO "F".
	IF NEWSTAGE {SET NEWSTAGEVALUE TO "T".}.
	LOG " " + NEWSTAGEVALUE + OBT:BODY:NAME:SUBSTRING(0, 2) + VWIDTH(-OBT:BODY:POSITION, 22) +
		VWIDTH(VELOCITY:ORBIT, 22) + VWIDTH(SHIP:SENSORS:ACC, 22) TO "test.tlmtr".
	LOG " " + FWIDTH(MISSIONTIME, 14) + VWIDTH(UP:FOREVECTOR, 8) + VWIDTH(FACING:FOREVECTOR, 8) +
		VWIDTH(ANGULARMOMENTUM, 8) + FWIDTH(MASS, 23) + FWIDTH(ISP(), 23) + FWIDTH(SHIP:Q, 23)
		TO "test.tlmtr".
}.

PRINT "T-10".
PRINT "LOCKING STEERING".
LOCK STEERING TO UP.
WAIT 1.
PRINT "T-9".
SET LOGDIR TO "0:/data/sotest/" + SHIPNAME + "/" + ROUND(TIME:SECONDS).
PRINT "WILL LOG TO " + LOGDIR.
WAIT 1.
PRINT "T-8".
DELETEPATH(LOGDIR + "/rl.log").
CREATE(LOGDIR + "/rl.log").
PRINT "CREATED RUNLEVEL LOG".
WAIT 1.
PRINT "T-7".
CD(LOGDIR).
PRINT "CHANGED TO LOG DIRECTORY".
WAIT 1.
PRINT "T-6".
DELETEPATH("test.tlmtr").
CREATE("test.tlmtr").
PRINT "CREATED TELEMETRY LOG".
WAIT 1.
PRINT "T-5".
LOG " sotest01" TO "test.tlmtr".
LOG " " + SHIPNAME:PADRIGHT(20) + " " + "T" TO "test.tlmtr".
LOG " " + SHIPNAME:PADRIGHT(20) + " " + FWIDTH(TIME:SECONDS + 5, 23) + " " + "S" TO "test.tlmtr".
PRINT "LOGGED HEADER".
WAIT 1.
PRINT "T-4".
LOCK THROTTLE TO 1.0.
PRINT "THROTTLED UP".
WAIT 1.
PRINT "T-3".
STAGE.
PRINT "STARTING ENGINE".
WAIT 1.
PRINT "T-2".
RCS ON.
PRINT "ENABLED RCS".
WAIT 1.
PRINT "T-1".
SET RUNLEVEL TO 1.
SET LOGGRAN TO 0.1.
SET TURNSTART TO 20.
SET ANG0 TO 5.
SET FINALSTAGE TO 1.
WAIT 1.

STAGE.
PRINT "CLAMPS RELEASED, LOGGING STARTED".
LOGSTEP(TRUE).
SET LASTTHRUST TO MAXTHRUST.

FUNCTION SETRL {
	PARAMETER NEWRL.
	LOG "T+" + ROUND(MISSIONTIME) + " RUNLEVEL " + RUNLEVEL + " -> " + NEWRL TO "rl.log".
	SET RUNLEVEL TO NEWRL.
}.

SET LOGCOUNTDOWN TO LOGGRAN.
SET JUSTSTAGED TO FALSE.

WHEN MAXTHRUST < 0.08 OR (LASTTHRUST - MAXTHRUST) / MAXTHRUST > 0.1 AND
		STAGE:READY AND STAGE <> FINALSTAGE THEN {
	PRINT "STAGING".
	SET JUSTSTAGED TO TRUE.
	SET LOGCOUNTDOWN TO -1.
	STAGE.
	SET LASTTHRUST TO MAXTHRUST.
	PRESERVE.
}.

SET LASTSTEP TO TIME:SECONDS.

UNTIL RUNLEVEL = 0 {
	IF RUNLEVEL = 1 {
		IF ALT:RADAR > 10 { // FIRST 10 METRES
			PRINT "LIFTOFF CONFIRMED".
			SET COUNTDOWN TO TURNSTART.
			SETRL(2).
		}
	} ELSE IF RUNLEVEL = 2 { // INITIAL ASCENT
		SET COUNTDOWN TO COUNTDOWN + (LASTSTEP - TIME:SECONDS).
		IF COUNTDOWN <= 0 {
			PRINT "INITIATING GRAVITY TURN".
			LOCK STEERING TO HEADING(90, 90 - ANG0).
			SETRL(3).
		}.
	} ELSE IF RUNLEVEL = 3 { // GRAVITY TURN ENGAGEMENT
		IF VCRS(SRFPROGRADE:FOREVECTOR, HEADING(90, 90 - ANG0):FOREVECTOR):MAG < 0.008 {
			PRINT "ENGAGING GRAVITY TURN".
			LOCK STEERING TO SRFPROGRADE.
			SETRL(4).
		}.
	} ELSE IF RUNLEVEL = 4 { // SURFACE GRAVITY TURN
		IF ALTITUDE > 86000 {
			PRINT "IN MESOPAUSE, SWITCHING TO ORBITAL VELOCITY GRAVITY TURN".
			LOCK STEERING TO PROGRADE.
			SETRL(5).
		}.
	} ELSE IF RUNLEVEL = 5 { // ORBITAL GRAVITY TURN
		IF ALTITUDE < 86000 {
			PRINT "BELOW MESOPAUSE, SWITCHING TO SURFACE VELOCITY GRAVITY TURN".
			LOCK STEERING TO SRFPROGRADE.
			SETRL(4).
		}.
	}.

	SET LOGCOUNTDOWN TO LOGCOUNTDOWN + (LASTSTEP - TIME:SECONDS).
	IF LOGCOUNTDOWN <= 0 {
		SET LOGCOUNTDOWN TO LOGGRAN.
		LOGSTEP(JUSTSTAGED).
		SET JUSTSTAGED TO FALSE.
	}.
	SET LASTSTEP TO TIME:SECONDS.
	SET LASTTHRUST TO MAXTHRUST.
	WAIT 0.
}.
