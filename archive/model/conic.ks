// FINDING STUFF OUT ABOUT OSCULATING CONIC ORBITS
RUNONCEPATH("0:/lib/hyperbolic.ks").

// FIND THE LINEAR ECCENTRICITY
FUNCTION CONIC_ECCLIN {
	PARAMETER SMA.
	PARAMETER ECC.
	RETURN SMA * ECC.
}.

// FIND THE FOCAL PARAMETER
FUNCTION CONIC_FOCPAR {
	PARAMETER SMA.
	PARAMETER ECC.
	RETURN SMA/ECC - SMA*ECC.
}.

// FIND THE SEMI LATUS RECTIM
FUNCTION CONIC_SLR {
	PARAMETER SMA.
	PARAMETER ECC.
	RETURN SMA - SMA*ECC^2.
}.

// FIND THE SEMI-MAJOR AXIS
FUNCTION CONIC_SMA {
	PARAMETER POS.
	PARAMETER VEL.
	PARAMETER MU.
	RETURN -MU*0.5/(VEL:SQRMAGNITUDE*0.5 - MU/POS:MAG).
}.

// FIND THE ECCENTRICITY VECTOR
FUNCTION CONIC_ECCVEC {
	PARAMETER POS.
	PARAMETER VEL.
	PARAMETER MU.
	RETURN VCRS(VEL, VCRS(POS, VEL))/MU - POS:NORMALIZED.
}.

// FIND THE ECCENTRICITY
FUNCTION CONIC_ECC {
	PARAMETER POS.
	PARAMETER VEL.
	PARAMETER MU.
	RETURN CONIC_ECCVEC(POS, VEL, MU):MAG.
}.

// FIND THE INCLINATION
FUNCTION CONIC_INC {
	PARAMETER POS.
	PARAMETER VEL.
	PARAMETER MU.
	LOCAL H IS VCRS(POS, VEL):NORMALIZED.
	RETURN ARCCOS(H:Y).
}.

// FIND THE VECTOR TO THE ASCENDING NODE
FUNCTION CONIC_VAN {
	PARAMETER POS.
	PARAMETER VEL.
	PARAMETER MU.
	LOCAL H IS VCRS(POS, VEL).
	RETURN V(-H:Z, 0, H:X).
}.

// FIND THE LONGITUDE OF THE ASCENDING NODE RELATIVE TO X
FUNCTION CONIC_LAN {
	PARAMETER POS.
	PARAMETER VEL.
	PARAMETER MU.
	LOCAL N IS CONIC_VAN(POS, VEL, MU).
	IF N:Y >= 0 {
		RETURN ARCCOS(N:Z/N:MAG).
	} ELSE {
		RETURN 2*CONSTANTS:PI - ARCCOS(N:Z/N:MAG).
	}.
}.

// FIND THE ARGUMENT OF PERIAPSIS
FUNCTION CONIC_ARGPERI {
	PARAMETER POS.
	PARAMETER VEL.
	PARAMETER MU.
	LOCAL E IS CONIC_ECCVEC(POS, VEL, MU).
	LOCAL N IS CONIC_VAN(POS, VEL, MU).
	RETURN ARCCOS(N*E/N:MAG/E:MAG).
}.

// FIND THE TRUE ANOMOLY
FUNCTION CONIC_TRUEANOM {
	PARAMETER POS.
	PARAMETER VEL.
	PARAMETER MU.
	LOCAL EV IS CONIC_ECCVEC(POS, VEL, MU).
	LOCAL NU IS ARCCOS(EV*POS/(EV:MAG*POS:MAG)).
	IF NU >= 0 {
		RETURN NU.
	} ELSE {
		RETURN 2*CONSTANTS:PI - NU.
	}.
}.

// GROUP ALL ORBITAL ELEMENTS TOGETHER
FUNCTION CONIC_ELEMENTS {
	PARAMETER POS.
	PARAMETER VEL.
	PARAMETER MU.
	LOCAL OUT IS LEXICON().
	SET OUT:POS TO POS.
	SET OUT:VEL TO VEL.
	SET OUT:MU TO MU.
	SET OUT:SMA TO CONIC_SMA(POS, VEL, MU).
	SET OUT:ECCVEC TO CONIC_ECCVEC(POS, VEL, MU).
	SET OUT:ECC TO CONIC_ECC(POS, VEL, MU).
	SET OUT:INC TO CONIC_INC(POS, VEL, MU).
	SET OUT:VAN TO CONIC_VAN(POS, VEL, MU).
	SET OUT:LAN TO CONIC_LAN(POS, VEL, MU).
	SET OUT:ARGPERI TO CONIC_ARGPERI(POS, VEL, MU).
	SET OUT:TRUEANOM TO CONIC_TRUEANOM(POS, VEL, MU).
	RETURN OUT.
}.

// FIND THE ECCENTRIC ANOMOLY
FUNCTION CONIC_ECCANOM {
	PARAMETER ELEMS.
	LOCAL DOT IS ELEMS:POS*ELEMS:VEL.
	IF DOT = 0 {
		IF ELEMS:POS:MAG >= ELEMS:SMA {
			RETURN 180.
		} ELSE {
			RETURN 0.
		}.
	} ELSE IF DOT < 0 {
		RETURN -ARCCOS((1 - ELEMS:POS:MAG/ELEMS:SMA)/ELEMS:ECC).
	} ELSE {
		RETURN ARCCOS((1 - ELEMS:POS:MAG/ELEMS:SMA)/ELEMS:ECC).
	}.
}.

// FIND THE HYPERBOLIC ECCENTRIC ANOMOLY
FUNCTION CONIC_HYBANOM {
	PARAMETER ELEMS.
	LOCAL H IS ARCCOSH((COS(ELEMS:TRUEANOM) + ELEMS:ECC)/(1 + ELEMS:ECC*COS(ELEMS:TRUEANOM))).
	IF ELEMS:POS*ELEMS:VEL < 0 {
		RETURN -H.
	} ELSE {
		RETURN H.
	}.
}.

// FIND THE ORBITAL PERIOD
FUNCTION CONIC_PERIOD {
	PARAMETER SMA.
	PARAMETER MU.
	RETURN 2 * CONSTANTS:PI * SQRT(SMA^3 / MU).
}.
