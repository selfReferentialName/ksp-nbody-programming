LIBJSON = ../deps/json-fortran/lib/libjsonfortran.a
LIBPYPLOT = ../deps/pyplot-fortran/lib/libpyplot.a

FC = gfortran
FFLAGS = -Wall -I../deps/json-fortran/lib -I../deps/pyplot-fortran/lib -Wno-tabs -g -O3 -march=native -fno-finite-math-only -fno-trapping-math -fopenmp
DEPLIBS = ../deps/json-fortran/lib/lib*.a ../deps/pyplot-fortran/lib/lib*.a

MATHS_DEPS=constants.o
MATHS_OBJS=maths.o $(MATHS_DEPS)
PLANET_DEPS=$(MATHS_OBJS) constants.o
PLANET_OBJS=planet.o $(PLANET_DEPS)
ATMOSPHERE_DEPS=ode.o constants.o $(PLANET_OBJS)
ATMOSPHERE_OBJS=atmosphere.o $(ATMOSPHERE_DEPS)
SOUNDING_DEPS=kosjson.o constants.o $(ATMOSPHERE_OBJS) $(DEPLIBS) sort.o
SOUNDING_OBJS=sounding.o $(SOUNDING_DEPS)
ODEBENCH_DEPS=ode.o
ODEBENCH_OBJS=$(ODEBENCH_DEPS) odebench.o
SIM_SOUNDING_DEPS=constants.o $(ATMOSPHERE_OBJS) $(DEPLIBS) planet.o
SIM_SOUNDING_OBJS=sim-sounding.o $(SIM_SOUNDING_DEPS)
TELEMETRY_DEPS=constants.o $(ATMOSPHERE_OBJS) sort.o $(DEPLIBS)
TELEMETRY_OBJS=telemetry.o $(TELEMETRY_DEPS)
GRAVTURN_DEPS=constants.o $(ATMOSPHERE_OBJS)
GRAVTURN_OBJS=gravturn.o $(GRAVTURN_DEPS)

default:
	echo "Please only use this to make a specific command"

%.o: %.f90
	$(FC) -c $(FFLAGS) $< -o $@

maths.o: maths.f90 $(MATHS_DEPS)
	$(FC) -c $(FFLAGS) $< -o $@

sounding.o: sounding.f90 $(SOUNDING_DEPS)
	$(FC) -c $(FFLAGS) sounding.f90 -o $@

odebench.o: odebench.f90 $(ODEBENCH_DEPS)
	$(FC) -c $(FFLAGS) -O0 odebench.f90 -o $@

planet.o: planet.f90 $(PLANET_DEPS)
	$(FC) -c $(FFLAGS) planet.f90 -o $@

atmosphere.o: atmosphere.f90 $(ATMOSPHERE_DEPS)
	$(FC) -c $(FFLAGS) atmosphere.f90 -o $@

sim-sounding.o: sim-sounding.f90 $(SIM_SOUNDING_DEPS)
	$(FC) -c $(FFLAGS) sim-sounding.f90 -o $@

gravturn.o: gravturn.f90 $(GRAVTURN_DEPS)
	$(FC) -c $(FFLAGS) $< -o $@

telemetry.o: telemetry.f90 $(TELEMETRY_DEPS)
	$(FC) -c $(FFLAGS) telemetry.f90 -o $@

sounding: $(SOUNDING_OBJS)
	$(FC) $(FFLAGS) $^ -o sounding

odebench: $(ODEBENCH_OBJS)
	$(FC) $(FFLAGS) $^ -o odebench

sim-sounding: $(SIM_SOUNDING_OBJS)
	$(FC) $(FFLAGS) $^ -o sim-sounding

telemetry: $(TELEMETRY_OBJS)
	$(FC) $(FFLAGS) $^ -o telemetry

gravturn: $(GRAVTURN_OBJS)
	$(FC) $(FFLAGS) $^ -o $@
