! atmospheric calculations -- density, pressure, temperature, and trajectory
MODULE atmosphere
	USE constants
	USE ode
	USE planet
	IMPLICIT NONE

	PRIVATE

	! fun fact: baseballs can travel up to 9% farther in Denver than at sea level
	! https://web.archive.org/web/20070222123604/http://www.pdas.com/bb1.htm

	! While normal KSP uses a truncated US standard atmosphere model for Kerbin
	! RSS gives Earth a crude extension of the international standard atmosphere
	! https://raw.githubusercontent.com/KSP-RO/RealSolarSystem/master/Technical%20Notes/Splines.pdf
	! I read the entire thing to get a sense of the model
	! One highlight: "This makes absolutely no sense. It gives a wonderful approximation.
	!     Honeybadger don't give a shit."
	! Anyways, the splines approximation given by RSS is within 0.001% of the standard
	! As such, this file just uses the standard atmosphere
	! except, of course, over 86km (Karman), where it uses the extended base equations of RSS

	! These functions get the atmospheric properties of earth at geopotential altitude.
	PUBLIC :: earthTempPot, earthPresPot, earthDordPot
	PUBLIC :: earthMetricToPot ! geometric to geopotential altutitude
	! Same as the *Pot functions, but using geometric altutude
	PUBLIC :: earthTemp, earthPres, earthDord

	! trajectory calculations
	PUBLIC :: earthGTLaunch ! gravity turn based launch on earth

	! layer max heights
	REAL*8, PARAMETER :: TrsMax = 11000
	REAL*8, PARAMETER :: TrpMax = 20000
	REAL*8, PARAMETER :: StslMax = 32000
	REAL*8, PARAMETER :: StshMax = 47000
	REAL*8, PARAMETER :: StpMax = 51000
	REAL*8, PARAMETER :: MslMax = 71000
	REAL*8, PARAMETER :: MshMax = 84852
	REAL*8, PARAMETER :: MpMax = 90000

	! layer temperature charge (K/m)
	REAL*8, PARAMETER :: TrsTD = -6.5e-3
	REAL*8, PARAMETER :: TrpTD = 0
	REAL*8, PARAMETER :: StslTD = 1e-3
	REAL*8, PARAMETER :: StshTD = 2.8e-3
	REAL*8, PARAMETER :: StpTD = 0
	REAL*8, PARAMETER :: MslTD = -2.8e-3
	REAL*8, PARAMETER :: MshTD = -2.0e-3
	REAL*8, PARAMETER :: MpTD = -2.0e-3
	REAL*8, PARAMETER :: ThTD = 1e-3

	! layer base temperature (K)
	REAL*8, PARAMETER :: TrsT0 = 275.15 + 19 + 610.0 * TrsTD
	REAL*8, PARAMETER :: TrpT0 = TrsT0 + TrsMax * TrsTD
	REAL*8, PARAMETER :: StslT0 = TrpT0 + (TrpMax - TrsMax) * TrpTD
	REAL*8, PARAMETER :: StshT0 = StslT0 + (StslMax - TrpMax) * StslTD
	REAL*8, PARAMETER :: StpT0 = StshT0 + (StshMax - StslMax) * StshTD
	REAL*8, PARAMETER :: MslT0 = StpT0 + (StpMax - StshMax) * StpTD
	REAL*8, PARAMETER :: MshT0 = MslT0 + (MslMax - StpMax) * MslTD
	REAL*8, PARAMETER :: MpT0 = MshT0 + (MshMax - MslMax) * MshTD
	REAL*8, PARAMETER :: ThT0 = MpT0 + (MpMax - MshMax) * ThTD

	INTEGER, PARAMETER :: GTLSize = 15

CONTAINS

	PURE FUNCTION earthTempPot(alt) RESULT(r)
		REAL*8, INTENT(IN) :: alt
		REAL*8 :: r

		IF (alt < TrsMax) THEN
			r = TrsT0 + alt * TrsTD
		ELSE IF (alt < TrpMax) THEN
			r = TrpT0 + (alt - TrsMax) * TrpTD
		ELSE IF (alt < StslMax) THEN
			r = StslT0 + (alt - TrpMax) * StslTD
		ELSE IF (alt < StshMax) THEN
			r = StshT0 + (alt - StslMax) * StshTD
		ELSE IF (alt < StpMax) THEN
			r = StpT0 + (alt - StshMax) * StpTD
		ELSE IF (alt < MslMax) THEN
			r = MslT0 + (alt - StpMax) * MslTD
		ELSE IF (alt < MshMax) THEN
			r = MshT0 + (alt - MslMax) * MshTD
		ELSE IF (alt < MpMax) THEN
			r = MpT0 + (alt - MshMax) * MpTD
		ELSE
			r = ThT0 + (alt - MpMax) * ThTD
		END IF
	END FUNCTION earthTempPot

	PURE FUNCTION earthPresPot(alt) RESULT(r)
		REAL*8, INTENT(IN) :: alt
		REAL*8 :: r

		! initial (or sea level) layer pressure
		REAL*8, PARAMETER :: Trs0 = AtmPa
		REAL*8, PARAMETER :: Trp0 = 22632
		REAL*8, PARAMETER :: Stsl0 = 5474.9
		REAL*8, PARAMETER :: Stsh0 = 868.02
		REAL*8, PARAMETER :: Stp0 = 110.91
		REAL*8, PARAMETER :: Msl0 = 66.939
		REAL*8, PARAMETER :: Msh0 = 3.9564
		REAL*8, PARAMETER :: Mp0 = 0.3734
		REAL*8, PARAMETER :: Th0 = 0.192739

		REAL*8 :: t0 ! initial temperature (K)
		REAL*8 :: td ! temperature rate of change (K/m)
		REAL*8 :: h0 ! layer base height
		REAL*8 :: p0 ! base pressure

		IF (alt < TrsMax) THEN
			h0 = 0
			t0 = TrsT0
			td = TrsTD
			p0 = Trs0
		ELSE IF (alt < TrpMax) THEN
			h0 = TrsMax
			p0 = Trp0
			r = p0 * E ** (-G0 * (alt - h0) / (EarthRSpec * TrpT0))
			RETURN
		ELSE IF (alt < StslMax) THEN
			h0 = TrpMax
			t0 = StslT0
			td = StslTD
			p0 = Stsl0
		ELSE IF (alt < StshMax) THEN
			h0 = StslMax
			t0 = StshT0
			td = StshTD
			p0 = Stsh0
		ELSE IF (alt < StpMax) THEN
			h0 = StshMax
			p0 = Stp0
			r = p0 * E ** (-G0 * (alt - h0) / (EarthRSpec * StpT0))
			RETURN
		ELSE IF (alt < MslMax) THEN
			h0 = StpMax
			t0 = MslT0
			td = MslTD
			p0 = Msl0
		ELSE IF (alt < MshMax) THEN
			h0 = MslMax
			t0 = MshT0
			td = MshTD
			p0 = Msh0
		ELSE IF (alt < MpMax) THEN
			h0 = MshMax
			t0 = MpT0
			td = MpTD
			p0 = Mp0
		ELSE
			h0 = MpMax
			t0 = ThT0
			td = ThTD
			p0 = Th0
		END IF

		! if we're here, td != 0
		r = p0 * (t0 / (t0 + (alt - h0) * td)) ** (G0 / (EarthRSpec * td))
	END FUNCTION earthPresPot

	PURE FUNCTION earthDordPot(alt) RESULT(r)
		REAL*8, INTENT(IN) :: alt
		REAL*8 :: r

		! TODO: optimise
		r = earthPresPot(alt) / earthTempPot(alt) / EarthRSpec
	END FUNCTION earthDordPot

	PURE FUNCTION earthMetricToPot(alt) RESULT(r)
		REAL*8, INTENT(IN) :: alt
		REAL*8 :: r
		r = EarthRad * alt / (EarthRad + alt)
	END FUNCTION

	PURE FUNCTION earthTemp(alt) RESULT(r)
		REAL*8, INTENT(IN) :: alt
		REAL*8 :: r
		r = earthTempPot(earthMetricToPot(alt))
	END FUNCTION

	PURE FUNCTION earthPres(alt) RESULT(r)
		REAL*8, INTENT(IN) :: alt
		REAL*8 :: r
		r = earthPresPot(earthMetricToPot(alt))
	END FUNCTION

	PURE FUNCTION earthDord(alt) RESULT(r)
		REAL*8, INTENT(IN) :: alt
		REAL*8 :: r
		r = earthDordPot(earthMetricToPot(alt))
	END FUNCTION

	! earthGTLaunch derivative helper
	! 1:3 is position, 4:6 is velocity, 7 is mass, 8:10 is turn info, 11:GTLDSize is stage info
	! 8 is startTime, 9 is turnRate, 10 is ang0
	! 11 is cda, 12 is isp0, 13 is isp1, 14 is burnRate, 15 is engageProduct or NaN if engaged
	! stage info has isp converted to escape velocity
	PURE SUBROUTINE earthGTLD(y, t, r)
		REAL*8, DIMENSION(:), INTENT(IN) :: y
		REAL*8, INTENT(IN) :: t
		REAL*8, DIMENSION(:), INTENT(OUT) :: r

		REAL*8, DIMENSION(3) :: facing, dp, airSpeed
		REAL*8 :: escVel, theta, pres, potAlt, dord, presAtm

		r = 0 ! most stuff doesn't change

		IF (t < y(8)) THEN
			facing = y(1:3)
		ELSE IF (y(15) /= y(15)) THEN
			facing = y(4:6) - baseAirSpeed(y(1:3))
		ELSE IF (t < y(8) + y(10) / y(9)) THEN
			theta = y(9) * (t - y(8))
			facing(1) = y(1) * cos(theta) - y(3) * sin(theta)
			facing(2) = y(2)
			facing(3) = y(1) * sin(theta) + y(3) * cos(theta)
		ELSE
			facing(1) = y(1) * cos(y(10)) - y(3) * sin(y(10))
			facing(2) = y(2)
			facing(3) = y(1) * sin(y(10)) + y(3) * cos(y(10))
			facing = facing / norm2(facing)
			IF (dot_product(facing, y(4:6) / norm2(y(4:6))) < y(15)) THEN
				! gfortran doesn't like NaN, so I have to do this to force one
				! this only work if r(15) is 0 -- this makes it 0 / 0 AKA NaN
				r(15) = r(15) / r(15)
			END IF
		END IF
		facing = facing / norm2(facing)
		potAlt = earthMetricToPot(norm2(y(1:3)) - earthRad)
		pres = earthPresPot(potAlt)
		dord = pres / earthTempPot(potAlt) / EarthRspec
		presAtm = pres / AtmPa
		escVel = y(12) * (1 - presAtm) + y(13) * presAtm
		airSpeed = y(4:6) - baseAirSpeed(y(1:3))

		r(1:3) = y(4:6) ! copy over velocity
		r(4:6) = -EarthMu * y(7) * y(1:3) / (norm2(y(1:3)) ** 3) + & ! gravity
			escVel * facing * y(14) - & ! thrust
			y(11) * dord / 2.0 * norm2(airSpeed) * airSpeed ! drag
		r(4:6) = r(4:6) / y(7) ! convert force to acceleration
		r(7) = -y(14) ! copy over burn rate
	END SUBROUTINE earthGTLD

	! calculate position just after passing EarthAtmMax or burnTime, whichever comes first
	! positions are inertial centred on earth, y is north, and z starts out at 0 (east)
	! all units are in terms of kg,m,s,K,rad
	! calculated to an accuracy of at least 1cm and 1cm/s
	! will end somewhere between EarthAtmMax and 100 meters above EarthAtmMax
	! INPUTS:
	! upTime is time spent facing directly upwards
	! turnRate is the (assumed constant) angular velocity (rad/s) when doing the gravity turn
	! engageProduct is the maximum dot product to engage the gravity turn
	! ang0 is the initial angle of the turn -- the angle to up after upTime * engageTime
	! alt0 is the initial altitude above sea level
	! lat0 is the latitude of the launch site (IN RADIANS!)
	! cda is the coefficiant of drag times the area
	! mass0 is the initial mass
	! isp0 is the vacuum specific impulse
	! isp1 is the sea level specific impulse
	! burnRate is the change of mass over time due to engines
	! burnTime could terminate the flight early
	! OUTPUTS:
	! endTime is the time (after 0) at which stuff is reported
	! endPos is the final position
	! endVel is the final velocity
	! OPTIONAL INPUTS:
	! y0 is the initial y value (take from steps)
	! prec is the maximum acceptable error in meters and meters per second
	! newStage signifies that the stage info parts of y0 should be overwritten
	! minCount is the initial stepCount
	! OPTIONAL OUTPUTS:
	! stepCount is the number of steps in the approximation
	! stepSize is the distance between steps in the approximation
	! steps is the value of the system at a given step (1:3 is position, 4:6 is velocity)
	! r is the final y value
	! exited is whether or not the stage exited the atmosphere
	PURE SUBROUTINE earthGTLaunch(startTime, turnRate, engageProduct, ang0, alt0, lat0, &
			cda, mass0, isp0, isp1, burnRate, burnTime, endTime, endPos, endVel, &
			y0, prec, newStage, minCount, &
			stepCount, stepSize, steps, r, exited)
		REAL*8, INTENT(IN) :: startTime, turnRate, engageProduct, burnTime
		REAL*8, INTENT(IN) :: ang0, alt0, lat0, cda, mass0, isp0, isp1, burnRate
		REAL*8, INTENT(OUT) :: endTime
		REAL*8, DIMENSION(3), INTENT(OUT) :: endPos, endVel
		INTEGER*8, OPTIONAL, INTENT(OUT) :: stepCount
		REAL*8, OPTIONAL, DIMENSION(:), INTENT(IN) :: y0
		REAL*8, OPTIONAL, INTENT(IN) :: prec
		LOGICAL, OPTIONAL, INTENT(IN) :: newStage
		INTEGER*8, OPTIONAL, INTENT(IN) :: minCount
		REAL*8, OPTIONAL, INTENT(OUT) :: stepSize
		REAL*8, OPTIONAL, DIMENSION(:, :), ALLOCATABLE, INTENT(OUT) :: steps
		REAL*8, OPTIONAL, DIMENSION(:), ALLOCATABLE, INTENT(OUT) :: r
		LOGICAL, OPTIONAL, INTENT(OUT) :: exited

		! we'll end somewhere in EarthAtmMax:MaxEndHeight
		REAL*8, PARAMETER :: MaxEndHeight = EarthAtmMax + 100

		REAL*8, PARAMETER :: T0 = 0
		REAL*8, PARAMETER :: EarlyErr = 10

		REAL*8 :: posErr
		REAL*8 :: velErr

		! 1:3 is position, 4:6 is velocity, 7 is mass, 8:10 is turn info, 11:14 is stage info
		REAL*8, DIMENSION(15) :: myY0, yEnd

		! rk4 arguments and temporaries
		REAL*8 :: h
		INTEGER*8 :: n
		REAL*8, DIMENSION(15) :: k1, k2, k3, k4, tmp, myR, errWeightPos, errWeightVel
		REAL*8, DIMENSION(:, :), ALLOCATABLE :: rOfI

		INTEGER*8 :: i

		! set error weights
		IF (present(prec)) THEN
			posErr = prec
			velErr = prec
		ELSE
			posErr = 0.01
			velErr = 0.01
		END IF
		errWeightPos = 0
		errWeightPos(1:3) = 1
		errWeightVel = 0
		errWeightVel(4:6) = 1

		IF (present(y0)) THEN
			myY0 = y0
			IF (present(newStage)) THEN
				IF (newStage) THEN
					myY0(7) = mass0
					myY0(11) = cda
					myY0(12) = isp0 * G0
					myY0(13) = isp1 * G0
					myY0(14) = burnRate
				END IF
			END IF
		ELSE
			! set initial position
			myY0(1) = cos(lat0) * (EarthRad + alt0)
			myY0(2) = sin(lat0) * (EarthRad + alt0)
			myY0(3) = 0

			! set initial velocity (due to the rotation of the earth)
			myY0(4:6) = baseAirSpeed(myY0(1:3))

			! set other starting arguments
			myY0(7) = mass0
			myY0(8) = startTime
			myY0(9) = turnRate
			myY0(10) = ang0
			myY0(11) = cda
			myY0(12) = isp0 * G0
			myY0(13) = isp1 * G0
			myY0(14) = burnRate
			myY0(15) = engageProduct
		END IF

		! get initial output estimate, notably including a good step size
		n = 8192
		IF (present(minCount)) n = minCount
		endTime = burnTime
		h = endTime / n
		CALL rk4GetStep(earthGTLD, myY0, T0, h, n, myR, EarlyErr, errWeightPos, k1, k2, k3, k4)

		! did the stage make it out of the atmosphere?
		IF (norm2(myR(1:3)) - EarthRad > EarthAtmMax) THEN ! if so, refine endTime
			ALLOCATE (rOfI(15, n))
			CALL rk4points(earthGTLD, myY0, T0, h, n, rOfI)
			DO i = 2, n
				IF (norm2(rOfI(1:3, i - 1)) - EarthRad > EarthAtmMax) THEN
					endTime = i * h
					n = i
					EXIT
				END IF
			END DO
			DEALLOCATE (rOfI)
		END IF

		! refine the step size, first by velocity, then position
		CALL rk4GetStep(earthGTLD, myY0, T0, h, n, myR, velErr, errWeightVel, k1, k2, k3, k4)
		n = 2 * n
		h = h / 2
		CALL rk4GetStep(earthGTLD, myY0, T0, h, n, myR, posErr, errWeightPos, k1, k2, k3, k4)

		! write output
		endPos = myR(1:3)
		endVel = myR(4:6)
		IF (present(stepCount)) stepCount = n
		IF (present(stepSize)) stepSize = n
		IF (present(steps)) THEN
			ALLOCATE (steps(size(myR), n + 1))
			CALL rk4points(earthGTLD, myY0, T0, h, n, steps)
		END IF
		IF (present(r)) THEN
			ALLOCATE (r(size(myR)))
			r = myR
		END IF
		IF (present(exited)) exited = (norm2(endPos) - EarthRad) >= EarthAtmMax
	END SUBROUTINE earthGTLaunch

END MODULE atmosphere
