! optimises gravity turns
! TODO: currently just optimises by velocity at exit or 100km/s + final altitude
PROGRAM gravturn

	USE atmosphere
	USE constants
	USE omp_lib ! it takes 100s to calculate once, let's parallelise

	IMPLICIT NONE

	INTERFACE
		PURE SUBROUTINE cost(ang0, startTime, stages, r)
			REAL*8, INTENT(IN) :: ang0, startTime
			REAL*8, DIMENSION(:, :), INTENT(IN) :: stages
			REAL*8, INTENT(OUT) :: r
		END SUBROUTINE cost
	END INTERFACE

	INTEGER, PARAMETER :: ThreadNum = 8 ! set to number of cores or threads

	CHARACTER(LEN=24) :: stageName, stageNameIn
	INTEGER :: stageCount
	REAL*8, DIMENSION(:, :), ALLOCATABLE :: stages

	REAL*8 :: ang0, startTime
	REAL*8 :: thisCost, lastCost, pa, pt, c0, cha, cht, a, m, t
	INTEGER :: i, j
	REAL*8, DIMENSION(ThreadNum) :: costTauToI

	REAL*8, PARAMETER :: HEpsilon = 1
	REAL*8, PARAMETER :: BigA = 10.0
	REAL*8, PARAMETER :: Cau = 0.5
	REAL*8, PARAMETER :: Tau = 0.5

	INTEGER, PARAMETER :: StagesUnit = 10
	CHARACTER(LEN=*), PARAMETER :: StagesFilename = 'stages'
	CHARACTER(LEN=*), PARAMETER :: StagesFormat = &
		'(A24,ES14.5e3,3ES12.4e3,ES14.6e3,ES12.4e3,ES10.2e3)'

!	CALL SYSTEM('clear')

	CALL omp_set_num_threads(ThreadNum)

	OPEN (StagesUnit, FILE=StagesFilename, ACTION='READ', POSITION='REWIND')

	PRINT *, 'How many stages?'
	READ *, stageCount
	ALLOCATE (stages(7, stageCount))
	DO i = 1, stageCount
		PRINT *, i, 'th stage name:'
		READ '(A)', stageName
		j = 24 - len_trim(stageName)
		stageName(j + 1:) = stageName(:len_trim(stageName))
		stageName(:j) = ' '
		REWIND StagesUnit
		READ (StagesUnit, *)
		DO
			READ (StagesUnit, StagesFormat) stageNameIn, stages(3, i), stages(1, i), &
				stages(5, i), stages(6, i), stages(2, i), stages(4, i), stages(7, i)
			IF (stageNameIn == stageName) EXIT
		END DO
	END DO

	ang0 = 5 * Pi / 180
	startTime = 35
	lastCost = 1
	thisCost = 1.1
	DO
		lastCost = thisCost
		!$omp parallel do
		DO i = 1, 3
			IF (i == 1) CALL cost(ang0, startTime, stages, c0)
			IF (i == 2) CALL cost(ang0 + HEpsilon, startTime, stages, cha)
			IF (i == 3) CALL cost(ang0, startTime + HEpsilon, stages, cht)
		END DO
		!$omp end parallel do
		pa = (c0 - cha) / HEpsilon
		pt = (c0 - cht) / HEpsilon
		IF (pa < 0 .and. pt < 0) THEN
			PRINT *, 'Gradiant best:', ang0, startTime, 'cost', c0
		ELSE IF (cha < cht) THEN
			PRINT *, 'Gradiant best:', ang0, startTime + HEpsilon, 'cost', cht
		ELSE ! cht <= cha
			PRINT *, 'Gradiant best:', ang0 + HEpsilon, 'cost', cha
		END IF
		PRINT *, c0, cht
		m = sqrt(pa ** 2 + pt ** 2)
		t = -Cau * m
		pa = pa / m
		pa = 0
		pt = pt / m
		a = 100.0
		! avoid NaN
		IF (.not. abs(pa) <= 1.0e30_8 .or. .not. abs(pt) <= 1.0e30_8 .or. &
				.not. m + 1.0e100_8 /= m .or. .not. t + 1.0e100_8 /= t) THEN
			PRINT *, pa, pt, m, t
			STOP 2
		END IF
		DO
			!$omp parallel do schedule(static, 1)
			DO i = 1, ThreadNum
				CALL cost(mod(ang0 + Tau ** (i - 1) * pa, 2 * Pi), &
					startTime + Tau ** (i - 1) * pt, stages, costTauToI(i))
			END DO
			!$omp end parallel do
			IF (costTauToI(1) - c0 < a * t) THEN
				thisCost = costTauToI(1)
				ang0 = mod(ang0 + pa * a, Pi * 2)
				startTime = startTime + pa * a
				GO TO 30
			END IF
			DO i = 2, ThreadNum
				IF (costTauToI(i) - costTauToI(i - 1) < a * t * Tau ** (i - 1)) THEN
					thisCost = costTauToI(i)
					ang0 = mod(ang0 + pa * a * Tau ** (i - 1), Pi * 2)
					startTime = startTime + pt * a * Tau ** (i - 1)
					GO TO 30
				END IF
			END DO
			j = 1
			DO i = 2, ThreadNum
				IF (costTauToI(i) < costTauToI(j)) j = i
			END DO
			PRINT *, 'Backtrack best', ang0 + pa * a * Tau ** (j - 1), &
				startTime + pt * a * Tau ** (j - 1), 'cost', costTauToI(j)
			a = a * Tau ** ThreadNum
		END DO
30		PRINT *, 'Current best:', ang0, startTime, 'cost', thisCost
		IF (lastCost == thisCost) STOP 0
	END DO
END PROGRAM gravturn

PURE SUBROUTINE cost(ang0, startTime, stages, r)
	USE atmosphere
	USE constants
	IMPLICIT NONE

	REAL*8, INTENT(IN) :: ang0, startTime
	REAL*8, DIMENSION(:, :), INTENT(IN) :: stages
	REAL*8, INTENT(OUT) :: r

	REAL*8, PARAMETER :: EngageProduct = 0.008
	REAL*8, PARAMETER :: Alt0 = 91
	REAL*8, PARAMETER :: Lat0 = 0.5

	REAL*8, PARAMETER :: PrecLast = 0.002
	REAL*8, PARAMETER :: PrecScale = 0.1

	REAL*8 :: endTime
	REAL*8, DIMENSION(3) :: endPos, endVel
	REAL*8, DIMENSION(:), ALLOCATABLE :: y, yLast
	LOGICAL :: exited

	INTEGER :: i

	! first stage
	CALL earthGTLaunch(startTime, stages(7, 1), EngageProduct, ang0, Alt0, Lat0, &
		stages(3, 1), stages(1, 1), stages(5, 1), stages(6, 1), stages(2, 1), &
		stages(4, 1), endTime, endPos, endVel, &
		prec=(PrecLast * PrecScale ** (size(stages, 2) - 1)), &
		r=y, exited=exited)
	CALL move_alloc(y, yLast)

	! subsequent stages
	DO i = 2, size(stages, 2)
		IF (norm2(endPos) /= norm2(endPos) .or. norm2(endVel) /= norm2(endVel)) THEN
			r = EarthAtmMax * EarthRad ** 10
			RETURN
		END IF
		IF (exited .or. norm2(endPos) > EarthAtmMax + EarthRad) EXIT
		CALL earthGTLaunch(startTime, stages(7, i), EngageProduct, ang0, &
			Alt0, Lat0, stages(3, i), stages(1, i), stages(5, i), &
			stages(6, i), stages(2, i), stages(4, i), endTime, endPos, endVel, &
			y0=yLast, prec=(PrecLast * PrecScale ** (i - 1)), newStage=.true., &
			r=y, exited=exited)
		CALL move_alloc(y, yLast)
	END DO

	IF (norm2(endPos) /= norm2(endPos) .or. norm2(endVel) /= norm2(endVel)) THEN
		r = EarthAtmMax * EarthRad ** 10
	ELSE IF (exited .or. norm2(endPos) > EarthAtmMax + EarthRad) THEN
		r = -norm2(endVel)
	ELSE
		r = abs(EarthAtmMax * 1000000 - norm2(endPos) + EarthRad * 1000)
	END IF
END SUBROUTINE cost
