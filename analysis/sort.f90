! sorting algorithms
MODULE sort
	IMPLICIT NONE

	PRIVATE

	! a subroutine of an input array and output array to sort in ascending lexical order
	! O(n) time, could use a large-constant O(n) of space
	! assumes nothing is NaN or infinity, properly sorts signed zero
	! TODO: fix
	PUBLIC :: radixSort
	INTERFACE radixSort
		MODULE PROCEDURE radixSort_real8
	END INTERFACE

	! another ascending lexical sort
	! O(n^2) worst case time, O(n) if almost sorted, O(1) extra space
	! handles infinity properly, assumes nothing is NaN
	PUBLIC :: gnomeSort
	INTERFACE gnomeSort
		MODULE PROCEDURE gnomeSort_real8
	END INTERFACE

CONTAINS

	SUBROUTINE gnomeSort_real8(input, output)
		REAL*8, DIMENSION(:), INTENT(IN) :: input
		REAL*8, DIMENSION(:), INTENT(OUT) :: output

		INTEGER :: i
		REAL*8 :: tmp

		i = 1
		output = input
		DO WHILE (i <= size(input))
			IF (i == 1) THEN
				i = i + 1
			ELSE IF (output(i) >= output(i - 1)) THEN
				i = i + 1
			ELSE
				tmp = output(i)
				output(i) = output(i - 1)
				output(i - 1) = tmp
			END IF
		END DO
	END SUBROUTINE gnomeSort_real8

	SUBROUTINE radixSort_real8(input, output)
		REAL*8, DIMENSION(:), INTENT(IN) :: input
		REAL*8, DIMENSION(:), INTENT(OUT) :: output

		INTEGER :: first, last, i

		REAL*8, DIMENSION(:), ALLOCATABLE :: tmp

		ALLOCATE (tmp(size(input)))

		! step 1: sort by sign into tmp
		first = 1
		last = size(input)
		DO i = 1, size(input)
			IF (input(i) < 0 .or. (input(i) == 0 .and. 1 / input(i) < 0)) THEN
				tmp(first) = input(i)
				first = first + 1
			ELSE
				tmp(last) = input(i)
				last = last - 1
			END IF
		END DO
		
		! sort the resulting subarrays
		CALL radixReal8PostSign(tmp(:first), output(:first), negative=.true.)
		CALL radixReal8PostSign(tmp(last:), output(last:), negative=.false.)
	END SUBROUTINE radixSort_real8

	SUBROUTINE radixReal8PostSign(input, output, negative)
		REAL*8, DIMENSION(:), INTENT(IN) :: input
		REAL*8, DIMENSION(:), INTENT(OUT) :: output
		LOGICAL, INTENT(IN) :: negative

		INTEGER, PARAMETER :: Bias = 1023 ! min exponent + Bias = 1
		INTEGER, PARAMETER :: BucketSize = 2048

		INTEGER :: i, idx
		INTEGER, DIMENSION(BucketSize) :: lengths, starts, positions
		REAL*8, DIMENSION(:), ALLOCATABLE :: tmp

		! don't do anything if there's nothing to do
		IF (size(input) == 0) RETURN

		ALLOCATE (tmp(size(input)))

		! step 2: count bucket sizes
		lengths = 0
		DO i = 1, size(input)
			idx = exponent(input(i)) + Bias
			IF (negative) idx = Bias * 2 + 1 - idx
			lengths(idx) = lengths(idx) + 1
		END DO

		! step 3: calculate bucket positions
		starts(1) = 1
		DO i = 2, BucketSize
			starts(i) = starts(i - 1) + lengths(i - 1)
		END DO

		! step 4: sort by exponent
		positions = starts
		DO i = 1, size(input)
			idx = exponent(input(i)) + Bias
			IF (negative) idx = Bias * 2 + 1 - idx
			tmp(positions(idx)) = input(i)
			positions(idx) = positions(idx) + 1
		END DO

		! sort the resulting subarrays
		DO i = 1, BucketSize
			CALL radixReal8Significand(tmp(starts(i):positions(i)), &
				output(starts(i):positions(i)), 39, negative)
		END DO
	END SUBROUTINE radixReal8PostSign

	RECURSIVE SUBROUTINE radixReal8Significand(input, output, shift, negative)
		REAL*8, DIMENSION(:), INTENT(IN) :: input
		REAL*8, DIMENSION(:), INTENT(OUT) :: output
		INTEGER, INTENT(IN) :: shift
		LOGICAL, INTENT(IN) :: negative

		INTEGER, PARAMETER :: BucketBits = 1
		INTEGER, PARAMETER :: BucketSize = 2

		INTEGER :: i, idx
		INTEGER, DIMENSION(BucketSize) :: lengths, starts, positions
		REAL*8, DIMENSION(:), ALLOCATABLE :: tmp

		! don't do anything if there's nothing to do
		IF (size(input) == 0) RETURN

		ALLOCATE (tmp(size(input)))

		! step 5: count bucket sizes
		lengths = 0
		DO i = 1, size(input)
			idx = ibits(int(fraction(input(i))), shift, BucketBits) + 1
			IF (negative) idx = BucketSize - idx + 2
			lengths(idx) = lengths(idx) + 1
		END DO

		! step 6: calculate bucket positions
		starts(1) = 1
		DO i = 2, BucketSize
			starts(i) = starts(i - 1) + lengths(i - 1)
		END DO

		! step 7: sort by significand
		positions = starts
		DO i = 1, size(input)
			idx = ibits(int(fraction(input(i))), shift, BucketBits) + 1
			IF (negative) idx = BucketSize - idx + 2
			tmp(positions(idx)) = input(i)
			positions(idx) = positions(idx) + 1
		END DO

		! sort the resulting subarrays if shift > 0, else return
		IF (shift > 0) THEN
			DO i = 1, BucketSize
				CALL radixReal8Significand(tmp(starts(i):positions(i)), &
					output(starts(i):positions(i)), shift - BucketBits, negative)
			END DO
		ELSE
			output = tmp
		END IF
	END SUBROUTINE radixReal8Significand

END MODULE sort
