! simulates an algorithm C launch in the vacuum flight phase
MODULE algc
	USE constants
	USE ode
	IMPLICIT NONE
	PRIVATE

	PUBLIC :: algcRaise, algcCirc

	INTEGER, PARAMETER :: RaiseCount = 14
	INTEGER, PARAMETER :: CircCount = 10

CONTAINS

	! 1:3 is position, 4:6 is velocity, 7 is mass, 8 is burn rate, 9:12 is RIFC*
	! 13 is BODY:MU, 14 is escape velocity
	! t is relative to RIFT0
	PURE SUBROUTINE raiseD(y, t, r)
		REAL*8, DIMENSION(:), INTENT(IN) :: y
		REAL*8, INTENT(IN) :: t
		REAL*8, DIMENSION(:), INTENT(OUT) :: r

		REAL*8 :: f, theta, tmr
		REAL*8, DIMENSION(3) :: facing

		f = y(9) * t ** 3 + y(10) * t ** 2 + y(11) * t + y(12)
		theta = acos(y(7) * (f + y(13) / norm2(y(1:3)) - norm2(y(4:6)) ** 2) / &
			norm2(y(1:3)) / y(14) / y(8))
		tmr = y(8) * y(14) / y(7)
		facing(1) = y(1) * cos(theta) - y(3) * sin(theta)
		facing(2) = 0
		facing(3) = y(1) * sin(theta) + y(3) * cos(theta)
		facing = facing / norm2(facing)

		r = 0
		r(1:3) = r(4:6)
		r(4:6) = -y(13) * y(1:3) / norm2(y(1:3)) ** 3 + tmr * facing
		r(7) = y(8)
	END SUBROUTINE raiseD
