! simulates a sounding rocket launch
PROGRAM sim_sounding
	USE constants
	USE atmosphere
	USE ode
	USE pyplot_module
	USE planet
	IMPLICIT NONE

	INTEGER, PARAMETER :: StageNameLen = 24 ! length of stage name field
	CHARACTER(LEN=*), PARAMETER :: StageFormat = '(A24,ES14.5E3,3ES12.4E3,ES14.6E3,ES12.4E3,ES10.2E3)'
	INTEGER, PARAMETER :: StageFile = 10
	CHARACTER(LEN=*), PARAMETER :: StageFilename = 'stages'

	! stage info
	REAL*8 :: cda, mass0, isp0, isp1, burnRate
	REAL*8 :: burnTime, turnRate

	CHARACTER(LEN=StageNameLen) :: stageNameIn, stageName

	REAL*8, PARAMETER :: StartTime = 30
	REAL*8, PARAMETER :: Ang0 = 5.0 / 180.0 * PI
	REAL*8, PARAMETER :: Alt0 = 91
	REAL*8, PARAMETER :: Lat0 = 28.608388900756836_8 / 180 * PI
	REAL*8, PARAMETER :: EngageProduct = 0.008
	INTEGER*8, PARAMETER :: MinCount = 100000

	REAL*8 :: endTime, stepSize
	REAL*8, DIMENSION(3) :: endPos, endVel
	REAL*8, DIMENSION(:), ALLOCATABLE :: y, yNext
	LOGICAL :: exited
	INTEGER*8 :: stepCount
	REAL*8, DIMENSION(:, :), ALLOCATABLE :: steps
	REAL*8, DIMENSION(:), ALLOCATABLE :: time, alt, orbitSpeed, srfSpeed, bas
	REAL*8, DIMENSION(3) :: pos, vel

	INTEGER :: i, istat, logSteps

	INTEGER*8, PARAMETER :: GraphCount = 10000
	INTEGER*8 :: graphDenom
	REAL*8 :: stageTime

	TYPE(pyplot) :: pltAlt, pltSrf, pltOrbit

	TYPE :: stageSimList
		REAL*8, DIMENSION(:, :), ALLOCATABLE :: steps
		REAL*8 :: endTime
		CHARACTER(LEN=StageNameLen) :: name
		TYPE(stageSimList), POINTER :: next
	END TYPE

	TYPE(stageSimList), POINTER :: stageSims, lastSim

	CALL SYSTEM('clear')

	stageName = '                       '
	WRITE (*, '(A)', ADVANCE='no') 'Stage name: '
	READ (*, '(A24)') stageNameIn
	stageName(StageNameLen - len_trim(stageNameIn) + 1:) = stageNameIn(:len_trim(stageNameIn))

	OPEN (StageFile, FILE=StageFilename, ACTION='READ', POSITION='REWIND')

	! the first line of stages doesn't hold any data, we need to skip it
	READ (StageFile, *)

	! get stage info
	DO
		READ (StageFile, StageFormat) stageNameIn, cda, mass0, isp0, isp1, burnRate, &
			burnTime, turnRate
		IF (stageNameIn == stageName) EXIT
		PRINT *, stageNameIn, stageName, len_trim(stageNameIn), len_trim(stageName)
	END DO

	! simulate
	CALL earthGTLaunch(StartTime, turnRate, EngageProduct, Ang0, Alt0, Lat0, cda, mass0, &
		isp0, isp1, burnRate, burnTime, endTime, endPos, endVel, minCount=MinCount, &
		stepCount=stepCount, stepSize=stepSize, steps=steps, r=y, exited=exited)
	ALLOCATE (stageSims)
	CALL move_alloc(steps, stageSims%steps)
	stageSims%name = stageName
	stageSims%endTime = endTime
	NULLIFY (stageSims%next)
	lastSim => stageSims
	DO WHILE (.not. exited)
		PRINT *, 'Stage final time', endTime, 'final altitude', norm2(endPos) - EarthRad, &
			'final velocity', norm2(endVel)
		PRINT *, 'Next stage name or blank for no last stage'
		READ (*, '(A24)') stageNameIn
		IF (len_trim(stageNameIn) == 0) EXIT
		REWIND StageFile
		READ (StageFile, *)
		stageName = '                       '
		stageName(StageNameLen - len_trim(stageNameIn) + 1:) = &
			stageNameIn(:len_trim(stageNameIn))
		DO
			READ (StageFile, StageFormat) stageNameIn, cda, mass0, isp0, isp1, &
				burnRate, burnTime, turnRate
			IF (stageNameIn == stageName) EXIT
			PRINT *, stageNameIn, stageName, len_trim(stageNameIn), len_trim(stageName)
		END DO
		CALL earthGTLaunch(StartTime, turnRate, EngageProduct, Ang0, Alt0, Lat0, &
			cda, mass0, isp0, isp1, burnRate, burnTime, endTime, endPos, endVel, &
			y0=y, minCount=MinCount, newStage=.true., stepCount=stepCount, &
			stepSize=stepSize, steps=steps, r=yNext, exited=exited)
		ALLOCATE (lastSim%next)
		lastSim => lastSim%next
		CALL move_alloc(steps, lastSim%steps)
		lastSim%name = stageName
		lastSim%endTime = endTime
		NULLIFY (lastSim%next)
		y = yNext
	END DO

	PRINT *, 'Expected exit time:', endTime
	PRINT *, 'Expected exit altitude:', norm2(endPos) - EarthRad
	PRINT *, 'Expected exit speed:', norm2(endVel)

	CLOSE (StageFile)

	CALL pltAlt%initialize(title='Simulation', legend=.true., &
		xlabel='Time (seconds)', ylabel='Altitude (m)')
	CALL pltSrf%initialize(title='Simulation', legend=.true., &
		xlabel='Time (seconds)', ylabel='Surface speed (m/s)')
	CALL pltOrbit%initialize(title='Simulation', legend=.true., &
		xlabel='Time (seconds)', ylabel='Orbital speed (m/s)')
	lastSim => stageSims
	stageTime = 0
	DO WHILE (associated(lastSim))
		stepCount = size(lastSim%steps, 2)
		graphDenom = max((stepCount - 1) / GraphCount, 1)
		logSteps = (stepCount - 1) / graphDenom - 1
		IF (allocated(time)) DEALLOCATE (time, alt, orbitSpeed, bas, srfSpeed)
		! zero indexed to make address indexing just be i * graphDenom + 1
		ALLOCATE (time(0:logSteps), alt(0:logSteps), orbitSpeed(0:logSteps))
		ALLOCATE (bas(0:logSteps), srfSpeed(0:logSteps))
		DO i = 0, logSteps
			pos = lastSim%steps(1:3, i * graphDenom + 1)
			vel = lastSim%steps(4:6, i * graphDenom + 1)
			time(i) = stageTime + lastSim%endTime * real(i) / real(logSteps)
			alt(i) = norm2(pos) - EarthRad
			orbitSpeed(i) = norm2(vel)
			srfSpeed(i) = norm2(vel - baseAirSpeed(pos))
		END DO
		stageName = lastSim%name
		CALL pltAlt%add_plot(time, alt, stageName, '-', istat=istat)
		CALL pltSrf%add_plot(time, srfSpeed, stageName, '-', istat=istat)
		CALL pltOrbit%add_plot(time, orbitSpeed, stageName, '-', istat=istat)
		stageTime = stageTime + lastSim%endTime
		lastSim => lastSim%next
	END DO
	CALL pltAlt%showFig(istat=istat)
	CALL pltSrf%showFig(istat=istat)
	CALL pltOrbit%showFig(istat=istat)

	READ * ! wait until something is typed to stop

END PROGRAM sim_sounding
