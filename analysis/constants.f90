! various physical constants, nice numbers, and other parameters
! most are sourced from Wikipedia, max atmosphere heights are from the RSS configs
MODULE constants
	PUBLIC

	! mathematical constants
	REAL*8, PARAMETER :: Pi = 4 * atan(1.0_8)
	REAL*8, PARAMETER :: E = 2.71828182845904523536_8

	! physical constants, in kg,m,s,K
	REAL*8, PARAMETER :: Grav = 6.67430e-11 ! newton's constant (m^3/kg/s^2)
	REAL*8, PARAMETER :: G0 = 9.80665 ! standard gravity (m/s^2)
	REAL*8, PARAMETER :: C = 299792458 ! speed of light (m/s)
	REAL*8, PARAMETER :: Bolz = 1.380649e-23 ! bolzman constant (J/K)

	! unit conversions
	REAL*8, PARAMETER :: AtmPa = 101325 ! pascals per atmosphere

	! celestial body numbers, in kg,m,s,K
	REAL*8, PARAMETER :: EarthRad = 6371009 ! mean radius of Earth (m) -- RSS
	REAL*8, PARAMETER :: EarthMass = 5.97237e24 ! mean mass of Earth (kg)
	REAL*8, PARAMETER :: EarthMu = EarthMass * Grav ! gravitation parameter of Earth (m/s^2/kg)
	REAL*8, PARAMETER :: Karman = 86e3 ! altitude at which space begins (end of std atm) (m)
	REAL*8, PARAMETER :: EarthAtmMax = 140e3 ! when the atmosphere is truncated (m)
	REAL*8, PARAMETER :: EarthRSpec = 287.058 ! Specific gas constant (J/kg/K)
	REAL*8, PARAMETER :: EarthRotSpeed = 7.292115e-5 ! earth's rotational speed (rad/s)
END MODULE constants
