! basic mathematical functions not already in Fortran08
MODULE maths
	USE constants
	IMPLICIT NONE

	PRIVATE

	! compute cross product
	PUBLIC :: cross
	INTERFACE cross
		MODULE PROCEDURE cross_real8
	END INTERFACE

CONTAINS

	PURE FUNCTION cross_real8(a, b) RESULT(r)
		REAL*8, INTENT(IN), DIMENSION(3) :: a, b
		REAL*8, DIMENSION(3) :: r

		r(1) = a(2) * b(3) - a(3) * b(2)
		r(2) = a(3) * b(1) - a(1) * b(3)
		r(3) = a(1) * b(2) - a(2) * b(1)
	END FUNCTION cross_real8

END MODULE maths
