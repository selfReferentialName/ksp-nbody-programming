! functions and subroutines to work with systems of ordinary differential equations
MODULE ode
	USE constants, C => speedOfLight
	IMPLICIT NONE

	PRIVATE
	PUBLIC :: rk4, rk4points, rk4getstep

CONTAINS

	! adaptive (variable step size) estimate of the IVP y' = f(y, t), y(t0) = y0 at tf
	! result is written to r, tf > t0
	! r, y0, and the output and input of f should all have the same size
	! global error at tf is kept approximately below maxErr
	! the error is weighted (as an absolute difference by dot_product) by errWeight
	! if the current step size is less than minErr * the optimum step size for maxErr,
	! the step size is increased
	! errWeight is optional (defaulting to even weighting)
	! minErr is optional (defaulting to effectively -Infinity)
	! https://numerary.readthedocs.io/en/latest/dormand-prince-method.html
	! https://en.wikipedia.org/wiki/Dormand%E2%80%93Prince_method
	! https://github.com/scipy/scipy/blob/master/scipy/integrate/dop/dopri5.f
	PURE SUBROUTINE rkdp(f, y0, t0, tf, r, maxErr, errWeight, minErr)
		INTERFACE
			PURE SUBROUTINE f(y, t, r)
				REAL*8, DIMENSION(:), INTENT(IN) :: y
				REAL*8, INTENT(IN) :: t
				REAL*8, DIMENSION(:), INTENT(OUT) :: r
			END SUBROUTINE f
		END INTERFACE
		REAL*8, DIMENSION(:), INTENT(IN) :: y0
		REAL*8, INTENT(IN) :: t0, tf
		REAL*8, DIMENSION(:), INTENT(OUT) :: r
		REAL*8, INTENT(IN) :: maxErr
		REAL*8, DIMENSION(:), INTENT(IN), OPTIONAL :: errWeight
		REAL*8, DIMENSION(:), INTENT(IN), OPTIONAL :: minErr

		REAL*8, DIMENSION(:, :), ALLOCATABLE :: k

		! Butcher's tableau, transposed so each, e.g, k comes from a column
		REAL*8, DIMENSION(7), PARAMETER :: C = (/0, 1/5, 3/10, 4/5, 8/9, 1, 1/)
		REAL*8, DIMENSION(6, 7), PARAMETER :: A = reshape((/ &
			0.,          0.,           0.,          0.,        0., 0., &
			1/5.,        0.,           0.,          0.,        0., 0., &
			3/40.,       9/40.,        0.,          0.,        0.,           0., &
			44/45.,      -56/15.,      32/9.,       0.,        0.,           0., &
			19372/6561., -25360/2187., 64448/6561., -212/729., 0.,           0., &
			9017/3168.,  −355/33.,     46732/5247., 49/176.,   −5103/18656., 0., &
			35/384.,     0.,           500/1113.,   125/192.,  −2187/6784.,  11/84. &
			/), (/6, 7/))
		REAL*8, DIMENSION(7, 2), PARAMETER :: B = reshape((/ &
			35/384.,     0., 500/1113.,   125/192., −2187/6784.,    11/84.,    0., &
			5179/57600., 0., 7571/16695., 393/640., −92097/339200., 187/2100., 1/40. &
			/), (/7, 2/))

	! estimate the IVP of y' = f(y, t), y(t0) = y0 at t0 + h * n, write result vector to r
	! r, y0, and the output and input of f should all have the same size
	! k1:k4, tmp are used to avoid allocating (this is designed to be called in hot loops)
	! they can be ommitted (all if k1 missing), in which case they will be allocated
	PURE SUBROUTINE rk4(f, y0, t0, h, n, r, k1i, k2i, k3i, k4i, tmpi)
		INTERFACE
			PURE SUBROUTINE f(y, t, r)
				REAL*8, DIMENSION(:), INTENT(IN) :: y
				REAL*8, INTENT(IN) :: t
				REAL*8, DIMENSION(:), INTENT(OUT) :: r
			END SUBROUTINE f
		END INTERFACE
		REAL*8, DIMENSION(:), INTENT(IN) :: y0
		REAL*8, INTENT(IN) :: t0, h
		INTEGER*8, INTENT(IN) :: n
		REAL*8, DIMENSION(:), INTENT(OUT) :: r
		REAL*8, DIMENSION(:), INTENT(OUT), OPTIONAL, TARGET :: k1i, k2i, k3i, k4i, tmpi

		REAL*8, DIMENSION(:), POINTER :: k1, k2, k3, k4, tmp

		INTEGER*8 :: i, j
		REAL*8 :: t

		IF (present(k1i)) THEN
			k1 => k1i
			k2 => k2i
			k3 => k3i
			k4 => k4i
			tmp => tmpi
		ELSE
			ALLOCATE (k1(size(r)), k2(size(r)), k3(size(r)), k4(size(r)), tmp(size(r)))
		END IF

		r = y0
		t = t0
		DO i = 1, n
			CALL f(r, t, k1)
			DO j = 1, size(k2)
				tmp(j) = r(j) + real(h) * k1(j) / 2.0
			END DO
			CALL f(tmp, t + h / 2.0, k2)
			DO j = 1, size(k3)
				tmp(j) = r(j) + real(h) * k2(j) / 2.0
			END DO
			CALL f(tmp, t + h / 2.0, k3)
			DO j = 1, size(k4)
				tmp(j) = r(j) + real(h) * k3(j)
			END DO
			CALL f(tmp, t + h, k4)
			r = r + h * (k1 + 2.0 * (k2 + k3) + k4) / 6.0
			t = t + h
		END DO

		IF (.not. present(k1i)) DEALLOCATE (k1, k2, k3, k4, tmp)
	END SUBROUTINE rk4

	! same as rk4 but writes the output as a matrix of column vectors
	! does not allocate r, assumes it has lower bound 1 and upper bound n + 1
	PURE SUBROUTINE rk4points(f, y0, t0, h, n, r)
		INTERFACE
			PURE SUBROUTINE f(y, t, r)
				REAL*8, DIMENSION(:), INTENT(IN) :: y
				REAL*8, INTENT(IN) :: t
				REAL*8, DIMENSION(:), INTENT(OUT) :: r
			END SUBROUTINE f
		END INTERFACE
		REAL*8, DIMENSION(:), INTENT(IN) :: y0
		REAL*8, INTENT(IN) :: t0, h
		INTEGER*8, INTENT(IN) :: n
		REAL*8, DIMENSION(:, :), INTENT(OUT) :: r

		REAL*8, DIMENSION(:), ALLOCATABLE :: k1, k2, k3, k4, tmp

		INTEGER*8 :: i, j
		REAL*8 :: t
		INTEGER*8 :: rows ! number of entries in, e.g. y0

		rows = size(y0)
		ALLOCATE (k1(rows), k2(rows), k3(rows), k4(rows), tmp(rows))

		r(:, 1) = y0
		t = t0
		DO i = 2, n
			CALL f(r(:, i - 1), t, k1)
			DO j = 1, size(k2)
				tmp(j) = r(j, i - 1) + real(h) * k1(j) / 2.0
			END DO
			CALL f(tmp, t + h / 2.0, k2)
			DO j = 1, size(k3)
				tmp(j) = r(j, i - 1) + real(h) * k2(j) / 2.0
			END DO
			CALL f(tmp, t + h / 2.0, k3)
			DO j = 1, size(k4)
				tmp(j) = r(j, i - 1) + real(h) * k3(j)
			END DO
			CALL f(tmp, t + h, k4)
			r(:, i) = r(:, i - 1) + h * (k1 + 2.0 * (k2 + k3) + k4) / 6.0
			t = t + h
		END DO
	END SUBROUTINE rk4points

	! like rk4 but it updates n and h to make sure that the most recent error
	! (weighted (by errweight or 1) sum of absolute deviations) is less than err
	! k1:k4 are optional, if missing, they will be allocated. if one is present, all should be
	PURE SUBROUTINE rk4getstep(f, y0, t0, h, n, r, err, errweight, k1, k2, k3, k4)
		INTERFACE
			PURE SUBROUTINE f(y, t, r)
				REAL*8, DIMENSION(:), INTENT(IN) :: y
				REAL*8, INTENT(IN) :: t
				REAL*8, DIMENSION(:), INTENT(OUT) :: r
			END SUBROUTINE f
		END INTERFACE
		REAL*8, DIMENSION(:), INTENT(IN) :: y0
		REAL*8, INTENT(IN) :: t0
		REAL*8, INTENT(INOUT) :: h
		INTEGER*8, INTENT(INOUT) :: n
		REAL*8, DIMENSION(:), INTENT(OUT) :: r
		REAL*8, INTENT(IN) :: err
		REAL*8, DIMENSION(:), INTENT(IN), OPTIONAL :: errweight
		REAL*8, DIMENSION(:), TARGET, OPTIONAL, INTENT(OUT) :: k1, k2, k3, k4

		REAL*8, DIMENSION(:), ALLOCATABLE :: last, errorweight, tmp
		REAL*8, DIMENSION(:), POINTER :: k1a, k2a, k3a, k4a

		ALLOCATE (last(size(y0)), errorweight(size(y0)), tmp(size(y0)))

		IF (present(errweight)) THEN
			errorweight = errweight
		ELSE
			errorweight = 1.0
		END IF

		IF (present(k1)) THEN
			k1a => k1
			k2a => k2
			k3a => k3
			k4a => k4
		ELSE
			ALLOCATE (k1a(size(y0)), k2a(size(y0)), k3a(size(y0)), k4a(size(y0)))
		END IF

		CALL rk4(f, y0, t0, h, n, r, k1a, k2a, k3a, k4a, tmp)
		last = r * 2.0 + err * 2.0

		DO WHILE (dot_product(errorweight, abs(r - last)) > err)
			last = r
			h = h / 2.0
			n = n * 2
			CALL rk4(f, y0, t0, h, n, r, k1a, k2a, k3a, k4a, tmp)
		END DO

		IF (.not. present(k1)) DEALLOCATE (k1a, k2a, k3a, k4a)
	END SUBROUTINE rk4getstep

END MODULE ode
