PROGRAM sounding

	USE kosjson
	USE atmosphere
	USE pyplot_module
	USE constants
	USE sort
	IMPLICIT NONE

	! each list of vectors is stored as an array of column vectors
	REAL(KIND=krk), ALLOCATABLE, DIMENSION(:, :) :: srfVel
	REAL(KIND=krk), ALLOCATABLE, DIMENSION(:, :) :: orbitVel
	REAL(KIND=krk), ALLOCATABLE, DIMENSION(:) :: altitude
	REAL(KIND=krk), ALLOCATABLE, DIMENSION(:) :: latitude
	REAL(KIND=krk), ALLOCATABLE, DIMENSION(:) :: longitud
	REAL(KIND=krk), ALLOCATABLE, DIMENSION(:, :) :: up
	REAL(KIND=krk), ALLOCATABLE, DIMENSION(:, :) :: facing
	REAL(KIND=krk), ALLOCATABLE, DIMENSION(:) :: mass
	REAL(KIND=krk), ALLOCATABLE, DIMENSION(:) :: dynPress
	REAL(KIND=krk), ALLOCATABLE, DIMENSION(:, :) :: angVel
	REAL(KIND=krk), ALLOCATABLE, DIMENSION(:, :) :: accel
!	INTEGER(KIND=kik), ALLOCATABLE, DIMENSION(:) :: stage
	CHARACTER(LEN=*), PARAMETER :: srfVelName   = 'srfvel  '
	CHARACTER(LEN=*), PARAMETER :: orbitVelName = 'orbitvel'
	CHARACTER(LEN=*), PARAMETER :: altitudeName = 'altitude'
	CHARACTER(LEN=*), PARAMETER :: latitudeName = 'latitude'
	CHARACTER(LEN=*), PARAMETER :: longitudName = 'longitud'
	CHARACTER(LEN=*), PARAMETER :: upName       = 'up      '
	CHARACTER(LEN=*), PARAMETER :: facingName   = 'facing  '
	CHARACTER(LEN=*), PARAMETER :: massName     = 'mass    '
	CHARACTER(LEN=*), PARAMETER :: dynPressName = 'dynpress'
	CHARACTER(LEN=*), PARAMETER :: pressureName = 'pressure'
	CHARACTER(LEN=*), PARAMETER :: angVelName   = 'angvel  '
	CHARACTER(LEN=*), PARAMETER :: isp0Name     = 'isp0    '
	CHARACTER(LEN=*), PARAMETER :: isp1Name     = 'isp1    '
	CHARACTER(LEN=*), PARAMETER :: accelName    = 'accel   '
	! length of each of the above
	INTEGER :: dataSize

	REAL(KIND=krk), ALLOCATABLE, DIMENSION(:) :: times
	REAL(KIND=krk), ALLOCATABLE, DIMENSION(:) :: srfSpeed, orbitSpeed, angSpeed
	REAL(KIND=krk), ALLOCATABLE, DIMENSION(:) :: pitch, estAccel

	INTEGER :: exitTime
	REAL(KIND=krk), ALLOCATABLE, DIMENSION(:) :: pressure, drag, cda, isp, dmdt, aoa, dmdtSort
	REAL(KIND=krk), ALLOCATABLE, DIMENSION(:) :: estDensity, density, estPressure, cdaSort

	REAL(KIND=krk), ALLOCATABLE, DIMENSION(:) :: isp0s, isp1s

	TYPE(kosvalueref) :: json
	TYPE(pyplot) :: plt
	INTEGER :: istat

	INTEGER :: i, j

	REAL(KIND=krk) :: isp1, isp0, estDensityNorm

	REAL(KIND=krk) :: TimeStep = 0.1

	CALL SYSTEM('clear')

	json = kosOfPath('sounding.json')
	SELECT TYPE (dat => json%dat)
	TYPE IS (kosLex)
		CALL convVecs(dat%get(srfVelName), srfVel)
		CALL convVecs(dat%get(orbitVelName), orbitVel)
		CALL convReals(dat%get(altitudeName), altitude)
		CALL convReals(dat%get(latitudeName), latitude)
		CALL convReals(dat%get(longitudName), longitud)
		CALL convVecs(dat%get(upName), up)
		CALL convVecs(dat%get(facingName), facing)
		CALL convReals(dat%get(massName), mass)
		CALL convReals(dat%get(dynPressName), dynPress)
		CALL convReals(dat%get(pressureName), pressure)
		CALL convVecs(dat%get(angVelName), angVel)
		CALL convReals(dat%get(isp0Name), isp0s)
		CALL convReals(dat%get(isp1Name), isp1s)
		CALL convVecs(dat%get(accelName), accel)
		dataSize = size(dynPress)
	CLASS DEFAULT
		CALL abort()
	END SELECT

	! convert units all in one place
	DO i = 1, dataSize
		mass(i) = mass(i) * 1000
		dynPress(i) = dynPress(i) * AtmPa
	END DO

	isp0 = isp0s(1)
	isp1 = isp1s(1)

	ALLOCATE (times(dataSize), srfSpeed(dataSize), orbitSpeed(dataSize), angSpeed(dataSize))
	ALLOCATE (pitch(dataSize), dmdt(dataSize), aoa(dataSize), estAccel(dataSize))
	DO i = 1, dataSize
		times(i) = real(i) * TimeStep
	END DO
	PRINT *, 'Logged until T+', dataSize

	CALL plt%initialize(title='Flight telemetry', &
			xlabel='Time (seconds)', ylabel='Surface speed (m/s)')
	DO i = 1, dataSize
		srfSpeed(i) = norm2(srfVel(:, i))
	END DO
	CALL plt%add_plot(times, srfSpeed, 'Surface speed (m/s)', '-', istat=istat)
	CALL plt%showFig(istat=istat)

	CALL plt%initialize(title='Flight telemetry', &
			xlabel='Time (seconds)', ylabel='Angle to up (deg)')
	DO i = 1, dataSize
		pitch(i) = acos(dot_product(facing(:, i), up(:, i)) / &
			(norm2(facing(:, i)) * norm2(up(:, i)))) * 180 / pi
		IF (pitch(i) /= pitch(i)) pitch(i) = 0.0
	END DO
	CALL plt%add_plot(times, pitch, 'Angle to up (deg/s)', '-', istat=istat)
	CALL plt%showFig(istat=istat)

	CALL plt%initialize(title='Flight telemetry', &
			xlabel='Time (seconds)', ylabel='Angular speed (deg/s)')
	DO i = 1, dataSize
		angSpeed(i) = norm2(angVel(:, i)) * 180 / pi
	END DO
	CALL plt%add_plot(times, angSpeed, 'Angular speed (deg/s)', '-', istat=istat)
	CALL plt%showFig(istat=istat)

	CALL plt%initialize(title='Flight telemetry', &
			xlabel='Time (seconds)', ylabel='Dynamic pressure (Pa)')
	CALL plt%add_plot(times, dynPress, 'Dynamic pressure (Pa)', '-', istat=istat)
	CALL plt%showFig(istat=istat)

	CALL plt%initialize(title='Flight telemetry', &
			xlabel='Time (seconds)', ylabel='Altitude (m)')
	CALL plt%add_plot(times, altitude, 'Altitude (m)', '-', istat=istat)
	CALL plt%showFig(istat=istat)

	DO i = 2, dataSize
		estAccel(i) = norm2(orbitVel(:, i - 1) - orbitVel(:, i)) / TimeStep
	END DO
	estAccel(1) = estAccel(2)
	CALL plt%initialize(title='Flight telemetry', legend=.true., &
			xlabel='Time (seconds)', ylabel='Accelleration (m/s^2)')
	CALL plt%add_plot(times, estAccel, 'Estimated from orbitvel', '-', istat=istat)
	CALL plt%add_plot(times, norm2(accel, 1), 'Actual from SHIP:SENSORS:ACC', '-', istat=istat)
	CALL plt%showFig(istat=istat)

	exitTime = dataSize
	DO i = 1, dataSize
		IF (altitude(i) > karman) THEN
			exitTime = i
			EXIT
		END IF
	END DO
	ALLOCATE (isp(exitTime), drag(exitTime), cda(exitTime), cdaSort(exitTime))
	ALLOCATE (estDensity(exitTime), density(exitTime), estPressure(exitTime), dmdtSort(exitTime))
	PRINT *, 'Exited atmosphere (or stopped logging) at T+', exitTime

	DO i = 1, exitTime
		density(i) = earthDord(altitude(i))
		isp(i) = isp0 * (1.0 - pressure(i)) + isp1 * pressure(i)
	END DO
	drag = -1
	cda = -1
	DO i = 2, exitTime
		drag(i) = mass(i) * norm2(accel(:, i)) - &
			earthmu * mass(i) / (earthrad + altitude(i)) ** 2 - &
			g0 * isp(i) * (mass(i) - mass(i - 1)) / TimeStep
		cda(i) = drag(i) / dynPress(i)
		IF (drag(i) + mass(i) == drag(i) .or. drag(i) /= drag(i)) drag(i) = -1
		IF (cda(i) + mass(i) == cda(i) .or. cda(i) /= cda(i)) cda(i) = -1
	END DO
	DO j = 1, 10
		IF (drag(exitTime) == -1) drag(exitTime) = drag(exitTime - 1)
		IF (cda(exitTime) == -1) cda(exitTime) = cda(exitTime - 1)
		DO i = 1, exitTime - 1
			IF (drag(i) == -1) drag(i) = drag(i + 1)
			IF (cda(i) == -1) cda(i) = cda(i + 1)
		END DO
	END DO
	pressure(1) = pressure(2)
	isp(1) = isp(2)
	DO i = 1, exitTime
		estDensity(i) = dynPress(i) / (norm2(srfSpeed) ** 8) / 2 / cda(i)
		estPressure(i) = earthPres(altitude(i)) / AtmPa
	END DO
	estDensityNorm = estDensity(60)
	DO i = 1, exitTime
		estDensity(i) = estDensity(i) * density(60) / estDensityNorm
	END DO

	CALL plt%initialize(title='Flight analysis', legend=.true., &
			xlabel='Altitude (m)', ylabel='Pressure (atm)')
	CALL plt%add_plot(altitude(1:exitTime), pressure(1:exitTime), 'Flight data', '-', istat=istat)
	CALL plt%add_plot(altitude(1:exitTime), estPressure, 'ISA', '-', istat=istat)
	CALL plt%showFig(istat=istat)
	CALL plt%initialize(title='Flight analysis', &
			xlabel='Altitude (m)', ylabel='Pressure error (atm)')
	CALL plt%add_plot(altitude(1:exitTime), pressure(1:exitTime) - estPressure, 'Flight data - ISA', '-', istat=istat)
	CALL plt%showFig(istat=istat)
	CALL plt%initialize(title='Flight analysis', &
			xlabel='Time (seconds)', ylabel='Drag (non-gravity or thrust accelleration) (kg*m/s^2)')
	CALL plt%add_plot(times(1:exitTime), drag, 'Drag', '-', istat=istat)
	CALL plt%showFig(istat=istat)
	CALL plt%initialize(title='Flight analysis', &
			xlabel='Time (seconds)', ylabel='Coefficiant of drag * cross section (m^2)')
	CALL plt%add_plot(times(1:exitTime), cda, 'Coefficiant of drag', '-', istat=istat)
	CALL plt%showFig(istat=istat)
	CALL plt%initialize(title='Flight analysis', legend=.true., &
			xlabel='Time (seconds)', ylabel='Density (kg/m^3)')
	CALL plt%add_plot(times(1:exitTime), estDensity, 'Estimated from data', '-', istat=istat)
	CALL plt%add_plot(times(1:exitTime), density, 'ISA model', '-', istat=istat)
	CALL plt%showFig(istat=istat)
	CALL gnomeSort(cda, cdaSort)
	PRINT *, 'Average coefficiant of drag', cdaSort(exitTime / 2)

	DO i = 1, dataSize
		IF (i /= 1) dmdt(i) = (mass(i) - mass(i - 1)) * 1000 / TimeStep
		aoa(i) = acos(dot_product(srfVel(:, i), facing(:, i)) / &
			norm2(srfVel(:, i)) / norm2(facing(:, i))) * 180 / Pi
		IF (aoa(i) /= aoa(i)) aoa(i) = 0
	END DO
	dmdt(1) = dmdt(2)

	CALL plt%initialize(title='Flight analysis', &
			xlabel='Time (seconds)', ylabel='Fuel flow (kg/s)')
	CALL plt%add_plot(times, dmdt, 'Fuel flow', '-', istat=istat)
	CALL plt%showFig(istat=istat)
	CALL plt%initialize(title='Flight analysis', &
			xlabel='Time (seconds)', ylabel='Angle of attack (deg)')
	CALL plt%add_plot(times, aoa, 'Aoa', '-', istat=istat)
	CALL plt%showFig(istat=istat)
	CALL gnomeSort(dmdt(1:exitTime), dmdtSort)
	PRINT *, 'Average fuel flow', dmdtSort(exitTime / 2)

	DO i = 1, size(isp0s)
		PRINT *, 'Stage', i, 'vacuum isp', isp0s(i), 'surface isp', isp1s(i)
	END DO

	READ *

END PROGRAM sounding
