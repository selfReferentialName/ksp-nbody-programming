PROGRAM telemetry

	USE constants
	USE planet
	USE sort
	USE pyplot_module
	IMPLICIT NONE

	CHARACTER(LEN=*), PARAMETER :: LogInfoFmt = '(1X,A8,I2.2)'
	CHARACTER(LEN=*), PARAMETER :: RocketInfoFmt = '(1X,A20,1X,L1)'
	CHARACTER(LEN=*), PARAMETER :: MissionInfoFmt = '(1X,A20,1X,ES23.16e3,1X,A1)'
	CHARACTER(LEN=*), PARAMETER :: LinePInfoFmt = '(1X,L1,A2,9ES22.16e2)'
	CHARACTER(LEN=*), PARAMETER :: LineRInfoFmt = '(1X,ES14.9e1,9ES8.5e1,3ES23.16e3)'

	INTEGER, PARAMETER :: TlmtrUnit = 10

	! log info length
	INTEGER :: dataSize, stageCount
	INTEGER, DIMENSION(:, :), ALLOCATABLE :: stages ! (/start, end/)

	! header info
	CHARACTER(LEN=8) :: scriptName
	CHARACTER(LEN=20) :: rocketName, missionName
	INTEGER :: version
	LOGICAL :: isFirstLaunch
	REAL*8 :: t0
	CHARACTER :: missionType
	INTEGER :: skipCount

	! post header logging
	LOGICAL, DIMENSION(:), ALLOCATABLE :: isNewStage
	CHARACTER(LEN=2), DIMENSION(:), ALLOCATABLE :: body
	REAL*8, DIMENSION(:, :), ALLOCATABLE :: pos, vel, acc
	REAL*8, DIMENSION(:, :), ALLOCATABLE :: up, facing, angMom
	REAL*8, DIMENSION(:), ALLOCATABLE :: mass, isp, q
	REAL*8, DIMENSION(:), ALLOCATABLE :: time

	! analysed data
	REAL*8, DIMENSION(:), ALLOCATABLE :: alt
	REAL*8, DIMENSION(:, :), ALLOCATABLE :: srfVel
	REAL*8, DIMENSION(:), ALLOCATABLE :: angUp, aoa

	! stage analysis
	REAL*8, DIMENSION(:), ALLOCATABLE :: cda, cdaSort, massFlow, massFlowSort

	CHARACTER(LEN=80) :: filename
	INTEGER :: i, j, idxS, idxE, istat
	CHARACTER(LEN=24), DIMENSION(:), ALLOCATABLE :: stageNames

	TYPE(pyplot) :: plt

	CALL SYSTEM('clear')

	PRINT *, 'File to analyse:'
	READ (*, '(A)') filename
	OPEN (TlmtrUnit, FILE=filename, ACTION='READ', POSITION='REWIND')

	! read the header
	READ (TlmtrUnit, LogInfoFmt) scriptName, version
	IF (version /= 1) THEN
		PRINT *, 'Bad version:', version
		CALL abort()
	END IF
	READ (TlmtrUnit, RocketInfoFmt) rocketName, isFirstLaunch
	READ (TlmtrUnit, MissionInfoFmt) missionName, t0, missionType
	skipCount = 3
	SELECT CASE (missionType)
	CASE ('S')
		! nothing to do
	CASE ('C')
		READ (TlmtrUnit, *) ! TODO: repeat info
		skipCount = 4
	CASE DEFAULT
		PRINT *, 'Bad mission type:', missionType
		CALL abort()
	END SELECT

	! read the data
	dataSize = 0
	DO ! get the file size
		READ (TlmtrUnit, *, END=10)
		READ (TlmtrUnit, *)
		dataSize = dataSize + 1
	END DO
10	ALLOCATE (isNewStage(dataSize), body(dataSize), pos(3, dataSize))
	ALLOCATE (vel(3, dataSize), acc(3, dataSize))
	ALLOCATE (up(3, dataSize), facing(3, dataSize), angMom(3, dataSize))
	ALLOCATE (mass(dataSize), isp(dataSize), q(dataSize), time(dataSize))
	REWIND TlmtrUnit
	DO i = 1, skipCount
		READ (TlmtrUnit, *)
	END DO
	DO i = 1, dataSize
		READ (TlmtrUnit, LinePInfoFmt) isNewStage(i), body(i), &
			pos(1, i), pos(2, i), pos(3, i), vel(1, i), vel(2, i), vel(3, i), &
			acc(1, i), acc(2, i), acc(3, i)
		READ (TlmtrUnit, LineRInfoFmt) time(i), up(1, i), up(2, i), up(3, i), &
			facing(1, i), facing(2, i), facing(3, i), &
			angMom(1, i), angMom(2, i), angMom(3, i), &
			mass(i), isp(i), q(i)
	END DO
	PRINT '('' Read '',I10,'' data points'')', dataSize
	PRINT '('' Data about rocket '',A20,'' logged by '',A8)', rocketName, scriptName
	PRINT '('' Mission '',A20,'' launched at 1951-01-01Z'',ES23.16e3)', missionName, t0
	IF (isFirstLaunch) THEN
		PRINT *, 'First launch (or test launch)'
	END IF
	SELECT CASE (missionType)
	CASE ('S')
		PRINT *, 'Suborbital mission'
	CASE DEFAULT
		PRINT *, 'TODO: print mission type'
	END SELECT

	! get stages
	stageCount = 0
	DO i = 1, dataSize
		IF (isNewStage(i)) stageCount = stageCount + 1
	END DO
	ALLOCATE (stages(2, stageCount))
	j = 0
	DO i = 1, dataSize
		IF (isNewStage(i)) THEN
			IF (.not. j == 0) stages(2, j) = i - 1
			j = j + 1
			stages(1, j) = i
		END IF
	END DO
	stages(2, stageCount) = dataSize
	PRINT '(''Rocket went through '',I10,'' stages'')', stageCount
	PRINT *, stages

	! get stage names
	ALLOCATE (stageNames(stageCount))
	PRINT *, 'Please name stages from first lit stage'
	DO i = 1, stageCount
		READ (*, '(A)') stageNames(i)
	END DO
	PRINT *, 'Thank you for naming the stages'

	ALLOCATE (alt(dataSize), srfVel(3, dataSize), angUp(dataSize), aoa(dataSize))

	CALL plt%initialize(title='Flight telemetry', legend=.true., &
		xlabel='MET (seconds)', ylabel='Altitude (m)')
	alt = norm2(pos, 1) - EarthRad
	DO i = 1, stageCount
		idxS = stages(1, i)
		idxE = stages(2, i)
		CALL plt%add_plot(time(idxS:idxE), alt(idxS:idxE), &
			stageNames(i), '-', istat=istat)
	END DO
	CALL plt%showFig(istat=istat)

	CALL plt%initialize(title='Flight telemetry', legend=.true., &
		xlabel='MET (seconds)', ylabel='Surface/air speed (m/s)')
	DO i = 1, dataSize
		srfVel(:, i) = vel(:, i) - baseAirSpeed(pos(:, i))
	END DO
	DO i = 1, stageCount
		idxS = stages(1, i)
		idxE = stages(2, i)
		CALL plt%add_plot(time(idxS:idxE), norm2(srfVel(:, idxS:idxE), 1), &
			stageNames(i), '-', istat=istat)
	END DO
	CALL plt%showFig(istat=istat)

	CALL plt%initialize(title='Flight telemetry', legend=.true., &
		xlabel='MET (seconds)', ylabel='Orbital speed (m/s)')
	DO i = 1, stageCount
		idxS = stages(1, i)
		idxE = stages(2, i)
		CALL plt%add_plot(time(idxS:idxE), norm2(vel(:, idxS:idxE), 1), &
			stageNames(i), '-', istat=istat)
	END DO
	CALL plt%showFig(istat=istat)

	CALL plt%initialize(title='Flight telemetry', legend=.true., &
		xlabel='MET (seconds)', ylabel='Acceleration (m/s^2)')
	DO i = 1, stageCount
		idxS = stages(1, i)
		idxE = stages(2, i)
		CALL plt%add_plot(time(idxS:idxE), norm2(acc(:, idxS:idxE), 1), &
			stageNames(i), '-', istat=istat)
	END DO
	CALL plt%showFig(istat=istat)

	CALL plt%initialize(title='Flight telemetry', legend=.true., &
		xlabel='MET (seconds)', ylabel='Angle to up (deg)')
	DO i = 1, dataSize
		angUp(i) = acos(dot_product(facing(:, i), up(:, i))) * 180 / Pi
		IF (angUp(i) /= angUp(i)) angUp(i) = 0
	END DO
	DO i = 1, stageCount
		idxS = stages(1, i)
		idxE = stages(2, i)
		CALL plt%add_plot(time(idxS:idxE), angUp(idxS:idxE), &
			stageNames(i), '-', istat=istat)
	END DO
	CALL plt%showFig(istat=istat)

	CALL plt%initialize(title='Flight telemetry', legend=.true., &
		xlabel='MET (seconds)', ylabel='Angle of attack (deg)')
	DO i = 1, dataSize
		aoa(i) = acos(dot_product(facing(:, i), srfVel(:, i)) / norm2(srfVel(:, i))) * 180 / Pi
		IF (aoa(i) /= aoa(i)) THEN
!			PRINT *, aoa(i), facing(:, i), vel(:, i)
			aoa(i) = 0
		END IF
	END DO
	DO i = 1, stageCount
		idxS = stages(1, i)
		idxE = stages(2, i)
		CALL plt%add_plot(time(idxS:idxE), aoa(idxS:idxE), &
			stageNames(i), '-', istat=istat)
	END DO
	CALL plt%showFig(istat=istat)

	CALL plt%initialize(title='Flight telemetry', legend=.true., &
		xlabel='MET (seconds)', ylabel='Dynamic pressure (Atm)')
	DO i = 1, stageCount
		idxS = stages(1, i)
		idxE = stages(2, i)
		CALL plt%add_plot(time(idxS:idxE), q(idxS:idxE), &
			stageNames(i), '-', istat=istat)
	END DO
	CALL plt%showFig(istat=istat)

	CALL plt%initialize(title='Flight telemetry', legend=.true., &
		xlabel='MET (seconds)', ylabel='Specific impulse (s)')
	DO i = 1, stageCount
		idxS = stages(1, i)
		idxE = stages(2, i)
		CALL plt%add_plot(time(idxS:idxE), isp(idxS:idxE), &
			stageNames(i), '-', istat=istat)
	END DO
	CALL plt%showFig(istat=istat)

	CALL plt%initialize(title='Flight telemetry', legend=.true., &
		xlabel='MET (seconds)', ylabel='Coefficiant of drag * area (m^2)')
	DO i = 1, stageCount
		idxS = stages(1, i) + 1
		idxE = stages(2, i)
		ALLOCATE (cda(idxS:idxE))
		DO j = idxS, idxE
			cda(j) = (mass(j) * norm2(acc(:, j)) + &
				EarthMu * mass(j) / norm2(pos(:, j)) ** 2 - &
				G0 * isp(j) * (mass(j - 1) - mass(j)) / (time(j) - time(j - 1))) &
				* 1000 / q(j) / AtmPa
			IF (cda(j) /= cda(j) .or. cda(j) > 1000.0) THEN
				PRINT *, 'Bad CDA:'
				PRINT *, cda(j), acc(:, j)
				PRINT *, isp(j), pos(:, j)
				PRINT *, mass(j - 1), mass(j), time(j - 1), time(j)
				idxE = j - 1
				EXIT
			END IF
		END DO
		ALLOCATE (cdaSort(1:(idxE - idxS + 1)))
		IF (cda(idxE) /= cda(idxE)) GO TO 20
		CALL plt%add_plot(time(idxS:idxE), cda(idxS:idxE), &
			stageNames(i), '-', istat=istat)
		CALL gnomeSort(cda(idxS:idxE), cdaSort)
		PRINT '(''Median cda for '',A24,'' = '',ES25.17e3)', &
			stageNames(i), cdaSort((idxE - idxS + 1) / 2)
20		DEALLOCATE (cda, cdaSort)
	END DO
	CALL plt%showFig(istat=istat)

	CALL plt%initialize(title='Flight telemetry', legend=.true., &
		xlabel='MET (seconds)', ylabel='Rate of change of mass (Mg/s)')
	DO i = 1, stageCount
		idxS = stages(1, i) + 1
		idxE = stages(2, i)
		ALLOCATE (massFlow(idxS:idxE))
		DO j = idxS, idxE
			massFlow(j) = (mass(j) - mass(j - 1)) / (time(j) - time(j - 1))
		END DO
		CALL plt%add_plot(time(idxS:idxE), massFlow(idxS:idxE), &
			stageNames(i), '-', istat=istat)
		ALLOCATE (massFlowSort(1:(idxE - idxS + 1)))
		CALL gnomeSort(massFlow(idxS:idxE), massFlowSort)
		PRINT '(''Median rate of mass change (kg/s) for '',A24,'' = '',ES25.17e3)', &
			stageNames(i), massFlowSort((idxE - idxS + 1) / 2)
		DEALLOCATE (massFlow, massFlowSort)
	END DO
	CALL plt%showFig(istat=istat)

	READ *

END PROGRAM telemetry
