! benchmark the ode module
PROGRAM odebench
	USE ode
	IMPLICIT NONE

	INTERFACE
		PURE SUBROUTINE id(y, t, r)
			REAL*8, DIMENSION(:), INTENT(IN) :: y
			REAL*8, INTENT(IN) :: t
			REAL*8, DIMENSION(:), INTENT(OUT) :: r
		END SUBROUTINE id
		PURE SUBROUTINE nbody(y, t, r)
			REAL*8, DIMENSION(:), INTENT(IN) :: y
			REAL*8, INTENT(IN) :: t
			REAL*8, DIMENSION(:), INTENT(OUT) :: r
		END SUBROUTINE nbody
		PURE SUBROUTINE tpi(y, t, r)
			REAL*8, DIMENSION(:), INTENT(IN) :: y
			REAL*8, INTENT(IN) :: t
			REAL*8, DIMENSION(:), INTENT(OUT) :: r
		END SUBROUTINE tpi
	END INTERFACE

	REAL*8, DIMENSION(3) :: y0_0, r_0, k1_0, k2_0, k3_0, k4_0, tmp_0
	REAL*8, DIMENSION(300) :: y0_2, r_2, k1_2, k2_2, k3_2, k4_2, tmp_2
	REAL*8, DIMENSION(3000) :: y0_3, r_3, k1_3, k2_3, k3_3, k4_3, tmp_3
	REAL*8, DIMENSION(30000) :: y0_4, r_4, k1_4, k2_4, k3_4, k4_4, tmp_4
	REAL*8, DIMENSION(3000000) :: y0_6, r_6, k1_6, k2_6, k3_6, k4_6, tmp_6

	INTEGER :: starttick, endtick, tickrate
	INTEGER :: i

	REAL*8, PARAMETER :: t0 = 0.0
	REAL*8, PARAMETER :: h = 0.1
	INTEGER*8, PARAMETER :: n = 100

	CALL system_clock(count_rate=tickrate)

	y0_0 = 1.0
	CALL system_clock(starttick)
	DO i = 1, 1000000
		CALL rk4(id, y0_0, t0, h, n, r_0, k1_0, k2_0, k3_0, k4_0, tmp_0)
	END DO
	CALL system_clock(endtick)
	PRINT *, '1e6 times, f=id, size(y0)=3e0, n=100:', real(endtick - starttick) / real(tickrate)

	y0_3 = 1.0
	CALL system_clock(starttick)
	DO i = 1, 1000
		CALL rk4(id, y0_3, t0, h, n, r_3, k1_3, k2_3, k3_3, k4_3, tmp_3)
	END DO
	CALL system_clock(endtick)
	PRINT *, '1e3 times, f=id, size(y0)=3e3, n=100:', real(endtick - starttick) / real(tickrate)

	y0_6 = 1.0
	CALL system_clock(starttick)
	DO i = 1, 1
		CALL rk4(id, y0_6, t0, h, n, r_6, k1_6, k2_6, k3_6, k4_6, tmp_6)
	END DO
	CALL system_clock(endtick)
	PRINT *, '1e0 times, f=id, size(y0)=3e6, n=100:', real(endtick - starttick) / real(tickrate)

	y0_0 = [(real(i), i = 1, 3)]
	CALL system_clock(starttick)
	DO i = 1, 10000
		CALL rk4(nbody, y0_0, t0, h, n, r_0, k1_0, k2_0, k3_0, k4_0, tmp_0)
	END DO
	CALL system_clock(endtick)
	PRINT *, '1e4 times, f=nbody, size(y0)=3e0, n=100:', real(endtick - starttick) / real(tickrate)

	y0_2 = [(real(i), i = 1, 300)]
	CALL system_clock(starttick)
	DO i = 1, 100
		CALL rk4(nbody, y0_2, t0, h, n, r_2, k1_2, k2_2, k3_2, k4_2, tmp_2)
	END DO
	CALL system_clock(endtick)
	PRINT *, '1e2 times, f=nbody, size(y0)=3e2, n=100:', real(endtick - starttick) / real(tickrate)

	y0_0 = [(real(i), i = 1, 3)]
	CALL system_clock(starttick)
	DO i = 1, 1000000
		CALL rk4(tpi, y0_0, t0, h, n, r_0, k1_0, k2_0, k3_0, k4_0, tmp_0)
	END DO
	CALL system_clock(endtick)
	PRINT *, '1e6 times, f=tpi, size(y0)=3e0, n=100:', real(endtick - starttick) / real(tickrate)

	y0_3 = [(real(i), i = 1, 3000)]
	CALL system_clock(starttick)
	DO i = 1, 1000
		CALL rk4(tpi, y0_3, t0, h, n, r_3, k1_3, k2_3, k3_3, k4_3, tmp_3)
	END DO
	CALL system_clock(endtick)
	PRINT *, '1e3 times, f=tpi, size(y0)=3e3, n=100:', real(endtick - starttick) / real(tickrate)

	y0_6 = [(real(i), i = 1, 3000000)]
	CALL system_clock(starttick)
	DO i = 1, 1
		CALL rk4(tpi, y0_6, t0, h, n, r_6, k1_6, k2_6, k3_6, k4_6, tmp_6)
	END DO
	CALL system_clock(endtick)
	PRINT *, '1e0 times, f=tpi, size(y0)=3e6, n=100:', real(endtick - starttick) / real(tickrate)

END PROGRAM odebench

PURE SUBROUTINE id(y, t, r)
	REAL*8, DIMENSION(:), INTENT(IN) :: y
	REAL*8, INTENT(IN) :: t
	REAL*8, DIMENSION(:), INTENT(OUT) :: r

	r = y
END SUBROUTINE id

PURE SUBROUTINE nbody(y, t, r)
	REAL*8, DIMENSION(:), INTENT(IN) :: y
	REAL*8, INTENT(IN) :: t
	REAL*8, DIMENSION(:), INTENT(OUT) :: r

	INTEGER :: i, j
	REAL*8, DIMENSION(3) :: diff

	r = y

	DO i = 1, size(y) / 3
		DO j = 1, size(y) / 3
			IF (i /= j) THEN
				diff = y(j:j+2) - y(i:i+2)
				r(i:i+2) = r(i:i+2) + &
					diff / norm2(diff) * &
					123.4 / norm2(diff) ** 2
			END IF
		END DO
	END DO
END SUBROUTINE nbody

PURE SUBROUTINE tpi(y, t, r)
	REAL*8, DIMENSION(:), INTENT(IN) :: y
	REAL*8, INTENT(IN) :: t
	REAL*8, DIMENSION(:), INTENT(OUT) :: r

	r = t
END SUBROUTINE tpi
