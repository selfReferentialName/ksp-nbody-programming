! miscalaneous planet related maths
MODULE planet
	USE constants
	USE maths
	IMPLICIT NONE

	PRIVATE

	PUBLIC :: vecLatitude, baseAirSpeed

CONTAINS

	! convert vector with y as north to latitude in radians
	PURE FUNCTION vecLatitude(v) RESULT(r)
		REAL*8, DIMENSION(3), INTENT(IN) :: v
		REAL*8, DIMENSION(3) :: r

		REAL*8, DIMENSION(3), PARAMETER :: up = (/0, 1, 0/)

		r = acos(dot_product(v, up) / norm2(v))
	END FUNCTION vecLatitude

	! calculate air speed of a given position with y as north
	PURE FUNCTION baseAirSpeed(v, rotSpeed) RESULT(r)
		REAL*8, DIMENSION(3), INTENT(IN) :: v ! position
		REAL*8, INTENT(IN), OPTIONAL :: rotSpeed ! speed at which the planet turns (rad/s)
		REAL*8, DIMENSION(3) :: r

		! get direction and distance from axis of rotation
		r(1) = -v(3)
		r(2) = 0
		r(3) = v(1)
		! get speed
		IF (present(rotSpeed)) THEN
			r = r * rotSpeed
		ELSE
			r = r * EarthRotSpeed
		END IF
	END FUNCTION baseAirSpeed

END MODULE planet
