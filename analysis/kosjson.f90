! KOS produced JSON parsing
MODULE kosjson

	USE json_kinds
	USE json_module
	IMPLICIT NONE
	PRIVATE

	CHARACTER(LEN=*), PARAMETER :: StringName = 'kOS.Safe.Encapsulation.StringValue'
	CHARACTER(LEN=*), PARAMETER :: VectorName = 'kOS.Suffixed.Vector'
	CHARACTER(LEN=*), PARAMETER :: ListName = 'kOS.Safe.Encapsulation.ListValue'
	CHARACTER(LEN=*), PARAMETER :: RealName = 'kOS.Safe.Encapsulation.ScalarDoubleValue'
	CHARACTER(LEN=*), PARAMETER :: IntName = 'kOS.Safe.Encapsulation.ScalarIntValue'
	CHARACTER(LEN=*), PARAMETER :: LexName = 'kOS.Safe.Encapsulation.Lexicon'

	INTEGER, PUBLIC, PARAMETER :: krk = selected_real_kind(10)
	INTEGER, PUBLIC, PARAMETER :: kik = selected_int_kind(9)

	! parsing functions
	PUBLIC :: kosOfJson, kosOfPath

	! specialised conversion functions
	PUBLIC :: convVecs, convReals, convInts

	TYPE, ABSTRACT, PUBLIC :: kosValue
	END TYPE

	TYPE, PUBLIC :: kosValueRef ! fortran can't do array of pointers
		CLASS(kosValue), POINTER, PUBLIC :: dat
	END TYPE

	TYPE, PUBLIC, EXTENDS(kosValue) :: kosString
		CHARACTER(KIND=CK, LEN=:), ALLOCATABLE, PUBLIC :: dat
	END TYPE

	TYPE, PUBLIC, EXTENDS(kosValue) :: kosVector
		REAL(KIND=krk), DIMENSION(3), PUBLIC :: dat
	END TYPE

	TYPE, PUBLIC, EXTENDS(kosValue) :: kosReal
		REAL(KIND=krk), PUBLIC :: dat
	END TYPE

	TYPE, PUBLIC, EXTENDS(kosValue) :: kosInt
		INTEGER(KIND=kik), PUBLIC :: dat
	END TYPE

	TYPE, PUBLIC, EXTENDS(kosValue) :: kosList
		TYPE(kosValueRef), DIMENSION(:), ALLOCATABLE, PUBLIC :: dat
	END TYPE

	TYPE, PUBLIC, EXTENDS(kosValue) :: kosLex
		CHARACTER(KIND=CK, LEN=8), ALLOCATABLE, PUBLIC :: keys(:)
		TYPE(kosValueRef), DIMENSION(:), ALLOCATABLE, PUBLIC :: values
		CONTAINS
			PROCEDURE :: get => get_lex
	END TYPE

	TYPE :: jsonvalueref
		TYPE(json_value), POINTER, PUBLIC :: p
	END TYPE

	! it seems the only way to read an array is to use global state
	! please don't touch this outside very specific places in
	! listOfJson and lexOfJson and the tempAddObj subroutine they call
	TYPE(jsonvalueref), POINTER, DIMENSION(:) :: temp_ptrs

CONTAINS

	FUNCTION jsonValueWrap(p) RESULT(r)
		TYPE(json_value), POINTER, INTENT(IN) :: p
		TYPE(jsonvalueref) :: r

		r%p => p
	END FUNCTION jsonValueWrap

	SUBROUTINE tempAddObj(json, element, i, count)
		CLASS(json_core), INTENT(INOUT) :: json ! unused
		TYPE(json_value), INTENT(IN), POINTER :: element
		INTEGER(KIND=IK), INTENT(IN) :: i, count

		IF (.not. associated(temp_ptrs)) THEN
			ALLOCATE (temp_ptrs(count))
		END IF

		temp_ptrs(i) = jsonValueWrap(element)
	END SUBROUTINE tempAddObj

	! try to get the value associated with a given key from a lexicon
	! if not found, it crashes
	FUNCTION get_lex(self, key) RESULT(dat)
		CLASS(kosLex), INTENT(IN) :: self
		CHARACTER(KIND=CK, LEN=*), INTENT(IN) :: key
		TYPE(kosValueRef) :: dat

		INTEGER :: i
		LOGICAL :: ok

		ok = .false.

		DO i = 1, size(self%keys)
			IF (self%keys(i) == key) THEN
				dat = self%values(i)
				ok = .true.
				EXIT
			END IF
		END DO
		IF (.not. ok) THEN
			PRINT *, 'Couldn''t find value for key:', key
			CALL abort()
		END IF
	END FUNCTION get_lex

	! $ is a path specifier, so we can't use the nice get subroutine
	! the workaround is a bit messy though, so I've put it here
	FUNCTION kostype(p, obj) RESULT(str)
		TYPE(json_value), POINTER, INTENT(IN) :: p
		TYPE(json_core), INTENT(INOUT) :: obj
		CHARACTER(KIND=CK, LEN=:), ALLOCATABLE :: str

		TYPE(json_value), POINTER :: strptr
		LOGICAL(KIND=LK) :: ok = .true.
		CHARACTER(KIND=CK, LEN=:), ALLOCATABLE :: errormsg

		CALL obj%get_child(p, '$type', strptr)
		CALL obj%check_for_errors(ok, errormsg)
		IF (.not. ok) THEN
			PRINT *, errormsg
			CALL abort()
		END IF
		CALL obj%get(strptr, str)
	END FUNCTION kostype

	FUNCTION stringOfJson(p, obj) RESULT(dat)
		TYPE(json_value), POINTER, INTENT(IN) :: p
		TYPE(json_core), INTENT(INOUT) :: obj
		TYPE(kosString) :: dat

		LOGICAL(KIND=LK) :: found

		CALL obj%get(p, 'value', dat%dat, found)
		IF (.not. found) CALL abort()
	END FUNCTION stringOfJson

	FUNCTION vectorOfJson(p, obj) RESULT(dat)
		TYPE(json_value), POINTER, INTENT(IN) :: p
		TYPE(json_core), INTENT(INOUT) :: obj
		TYPE(kosVector) :: dat

		LOGICAL(KIND=LK) :: found

		CALL obj%get(p, 'x', dat%dat(1), found)
		IF (.not. found) CALL abort()
		CALL obj%get(p, 'y', dat%dat(2), found)
		IF (.not. found) CALL abort()
		CALL obj%get(p, 'z', dat%dat(3), found)
		IF (.not. found) CALL abort()
	END FUNCTION vectorOfJson

	FUNCTION realOfJson(p, obj) RESULT(dat)
		TYPE(json_value), INTENT(IN), POINTER :: p
		TYPE(json_core), INTENT(INOUT) :: obj
		TYPE(kosReal) :: dat

		LOGICAL(KIND=LK) :: found

		CALL obj%get(p, 'value', dat%dat, found)
		IF (.not. found) CALL abort()
	END FUNCTION realOfJson

	FUNCTION intOfJson(p, obj) RESULT(dat)
		TYPE(json_value), INTENT(IN), POINTER :: p
		TYPE(json_core), INTENT(INOUT) :: obj
		TYPE(kosInt) :: dat

		LOGICAL(KIND=LK) :: found

		CALL obj%get(p, 'value', dat%dat, found)
		IF (.not. found) CALL abort()
	END FUNCTION intOfJson

	! not thread safe
	RECURSIVE FUNCTION listOfJson(p, obj) RESULT(dat)
		TYPE(json_value), INTENT(IN), POINTER :: p
		TYPE(json_core), INTENT(INOUT) :: obj
		TYPE(kosList) :: dat

		LOGICAL(KIND=LK) :: found
		TYPE(jsonvalueref), POINTER, DIMENSION(:) :: ptrs
		INTEGER :: i

		! not reentrant or thread safe starting here
		CALL obj%get(p, 'items', tempAddObj, found)
		IF (.not. found) CALL abort()
		ptrs => temp_ptrs
		NULLIFY (temp_ptrs)
		! should be reentrant again

		ALLOCATE (dat%dat(size(ptrs)))
		DO i = 1, size(ptrs)
			dat%dat(i) = kosOfCore(ptrs(i)%p, obj)
		END DO

		DEALLOCATE (ptrs)
	END FUNCTION listOfJson

	! not thread safe
	RECURSIVE FUNCTION lexOfJson(p, obj) RESULT(dat)
		TYPE(json_value), INTENT(IN), POINTER :: p
		TYPE(json_core), INTENT(INOUT) :: obj
		TYPE(kosLex) :: dat

		LOGICAL(KIND=LK) :: found
		TYPE(jsonvalueref), POINTER, DIMENSION(:) :: ptrs
		INTEGER :: i
		CHARACTER(KIND=CK, LEN=:), ALLOCATABLE :: tempstr

		! not reentrant or thread safe starting here
		CALL obj%get(p, 'entries', tempAddObj, found)
		IF (.not. found) CALL abort()
		ptrs => temp_ptrs
		NULLIFY (temp_ptrs)
		! should be reentrant again

		IF (modulo(size(ptrs), 2) /= 0) CALL abort()
		ALLOCATE (dat%keys(size(ptrs) / 2))
		ALLOCATE (dat%values(size(ptrs) / 2))
		DO i = 1, size(dat%values)
			tempstr = kostype(ptrs(i * 2 - 1)%p, obj)
			IF (tempstr /= StringName) THEN
				PRINT *, 'Key type was: ', tempstr, ' not: ', StringName
				CALL obj%print_error_message()
				CALL abort()
			END IF
			CALL obj%get(ptrs(i * 2 - 1)%p, 'value', tempstr, found)
			IF (.not. found) CALL abort()
			dat%keys(i) = tempstr(1:8) ! please don't have more
			dat%values(i) = kosOfCore(ptrs(i * 2)%p, obj)
		END DO

		DEALLOCATE (ptrs)
	END FUNCTION lexOfJson

	! not thread safe
	RECURSIVE FUNCTION kosOfCore(p, obj) RESULT(dat)
		TYPE(json_value), INTENT(IN), POINTER :: p
		TYPE(json_core), INTENT(INOUT) :: obj
		TYPE(kosValueRef) :: dat

		CHARACTER(KIND=CK, LEN=:), ALLOCATABLE :: typeName
		TYPE(kosString), POINTER :: str
		TYPE(kosVector), POINTER :: vec
		TYPE(kosReal), POINTER :: double
		TYPE(kosInt), POINTER :: intr
		TYPE(kosList), POINTER :: list
		TYPE(kosLex), POINTER :: lex

		typeName = kostype(p, obj)
		SELECT CASE (typeName)
		CASE (StringName)
			ALLOCATE (str)
			str = stringOfJson(p, obj)
			dat%dat => str
		CASE (VectorName)
			ALLOCATE (vec)
			vec = vectorOfJson(p, obj)
			dat%dat => vec
		CASE (RealName)
			ALLOCATE (double)
			double = realOfJson(p, obj)
			dat%dat => double
		CASE (IntName)
			ALLOCATE (intr)
			intr = intOfJson(p, obj)
			dat%dat => intr
		CASE (ListName)
			ALLOCATE (list)
			list = listOfJson(p, obj)
			dat%dat => list
		CASE (LexName)
			ALLOCATE (lex)
			lex = lexOfJson(p, obj)
			dat%dat => lex
		CASE DEFAULT
			PRINT *, 'Bad type name: ', typeName
			CALL obj%print_error_message()
			CALL abort()
		END SELECT
	END FUNCTION kosOfCore

	! not thread safe
	! parse a json_file into a kosValueRef
	! crashes when given bad data
	FUNCTION kosOfJson(json) RESULT(dat)
		TYPE(json_file), INTENT(INOUT) :: json
		TYPE(kosValueRef) :: dat

		TYPE(json_value), POINTER :: p
		TYPE(json_core) :: obj

		CALL json%get(p)
		CALL json%get_core(obj)
		dat = kosOfCore(p, obj)
	END FUNCTION kosOfJson

	! not thread safe
	! parse a file at path containing json into a kosValueRef
	! crashes when given bad data
	FUNCTION kosOfPath(path) RESULT(dat)
		CHARACTER(KIND=CDK, LEN=*), INTENT(IN) :: path
		TYPE(kosValueRef) :: dat

		TYPE(json_file) :: json

		CALL json%load(path)
		dat = kosOfJson(json)
	END FUNCTION kosOfPath

	! convert a kosList of kosvecs into a matrix of column vectors
	SUBROUTINE convVecs(raw, mat)
		TYPE(kosValueRef), INTENT(IN) :: raw
		REAL(KIND=krk), ALLOCATABLE, DIMENSION(:, :), INTENT(OUT) :: mat

		INTEGER :: i, j

		SELECT TYPE (list => raw%dat)
		TYPE IS (kosList)
			ALLOCATE (mat(3, size(list%dat)))
			DO i = 1, size(list%dat)
				SELECT TYPE (dat => list%dat(i)%dat)
				TYPE IS (kosVector)
					DO j = 1, 3
						mat(j, i) = dat%dat(j)
					END DO
				CLASS DEFAULT
					CALL abort()
				END SELECT
			END DO
		CLASS DEFAULT
			CALL abort()
		END SELECT
	END SUBROUTINE convVecs

	! convert a kosList of kosReals into a vector
	SUBROUTINE convReals(raw, vec)
		TYPE(kosValueRef), INTENT(IN) :: raw
		REAL(KIND=krk), ALLOCATABLE, DIMENSION(:), INTENT(OUT) :: vec

		INTEGER :: i

		SELECT TYPE (list => raw%dat)
		TYPE IS (kosList)
			ALLOCATE (vec(size(list%dat)))
			DO i = 1, size(list%dat)
				SELECT TYPE (dat => list%dat(i)%dat)
				TYPE IS (kosReal)
					vec(i) = dat%dat
				TYPE IS (kosInt)
					vec(i) = real(dat%dat)
				CLASS DEFAULT
					CALL abort()
				END SELECT
			END DO
		CLASS DEFAULT
			CALL abort()
		END SELECT
	END SUBROUTINE convReals

	! convert a kosList of kosInts into a vector
	SUBROUTINE convInts(raw, vec)
		TYPE(kosValueRef), INTENT(IN) :: raw
		INTEGER(KIND=kik), ALLOCATABLE, DIMENSION(:), INTENT(OUT) :: vec

		INTEGER :: i

		SELECT TYPE (list => raw%dat)
		TYPE IS (kosList)
			ALLOCATE (vec(size(list%dat)))
			DO i = 1, size(list%dat)
				SELECT TYPE (dat => list%dat(i)%dat)
				TYPE IS (kosInt)
					vec(i) = dat%dat
				CLASS DEFAULT
					CALL abort()
				END SELECT
			END DO
		CLASS DEFAULT
			CALL abort()
		END SELECT
	END SUBROUTINE convInts

END MODULE kosjson
